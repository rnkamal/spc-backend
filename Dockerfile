FROM openjdk:8-jdk-alpine

ADD spc-api.jar /apps/spc-api.jar

ENV JAVA_OPTS="-Dspring.profiles.active=${springProfile}"

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /apps/spc-api.jar" ]

