package com.aptar.spc.DTO.request;

import com.aptar.spc.entity.common.RevisionParamDetails;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class RevisionParamRequest {

	@JsonProperty("revisionParamId")
	private Integer id;
	@JsonProperty("parameterId")
	private Integer parameterId;
	@JsonProperty("combinationRevisionId")
	private Integer combinationRevisionId;
	@JsonProperty("spc")
	private boolean spc;
	@JsonProperty("revisionParamDetails")
	private RevisionParamDetails revisionParamDetails;
}
