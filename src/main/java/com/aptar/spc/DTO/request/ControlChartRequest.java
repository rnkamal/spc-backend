package com.aptar.spc.DTO.request;

import java.util.List;

import com.aptar.spc.DTO.response.Attribute;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ControlChartRequest {

	@JsonProperty("attribute")
	private List<Attribute> attribute;
}
