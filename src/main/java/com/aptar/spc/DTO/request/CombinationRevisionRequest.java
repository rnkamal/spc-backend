package com.aptar.spc.DTO.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class CombinationRevisionRequest {

	@JsonProperty("combinationId")
	private Integer id;
	@JsonProperty("revision")
	private Integer revision;
	@JsonProperty("createdBy")
	private String createdBy;
	@JsonProperty("reason")
	private String reason;
	@JsonProperty("active")
	private boolean active;

}
