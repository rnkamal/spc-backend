package com.aptar.spc.DTO.request;

import java.io.Serializable;

import lombok.Data;

@Data
public class KeyCloakUser implements Serializable {

	private static final long serialVersionUID = -1L;

	private String username;
	private String email;
	private String firstName;
	private String lastName;
	private String password;
}
