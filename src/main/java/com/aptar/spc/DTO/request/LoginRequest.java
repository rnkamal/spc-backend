package com.aptar.spc.DTO.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.ToString;

/**
 * View Model object for storing a user's credentials.
 */
@Data
@ToString(exclude = { "password" })
public class LoginRequest {

	private static final int MAX_LENGTH_USERNAME = 50;

	@NotBlank
	@Size(min = 1, max = MAX_LENGTH_USERNAME)
	private String username;

	@NotBlank
	private String password;

	private String legacyMesUrl;

}
