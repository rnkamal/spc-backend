package com.aptar.spc.DTO.request;

import java.util.List;

import javax.validation.Valid;

import com.aptar.spc.validation.interfaces.OneOf;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
@OneOf(message = "Site/FunctionalLocation is mandantory", condition = "notnull", value = { "Site",
		"FunctionalLocation" })
public class CombinationRequest {

	@JsonProperty("site")
	// @NotEmpty(message = "Site is mandatory")
	private List<@Valid Integer> site;
	@JsonProperty("functionalLocation")
	// @NotEmpty(message = "FunctionalLocation is mandatory")
	private List<@Valid Integer> functionalLocation;
}
