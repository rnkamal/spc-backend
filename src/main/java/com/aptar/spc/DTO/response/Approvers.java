package com.aptar.spc.DTO.response;

import java.io.Serializable;
import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Approvers implements Serializable {

	private List<Approver> Approvers;
}
