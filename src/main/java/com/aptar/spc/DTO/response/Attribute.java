package com.aptar.spc.DTO.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Attribute {

	@JsonProperty("value")
	private double attibuteValue;
	@JsonProperty("controlLimitStatus")
	private boolean controlLimitStatus;
}
