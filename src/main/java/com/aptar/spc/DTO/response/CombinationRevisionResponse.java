package com.aptar.spc.DTO.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class CombinationRevisionResponse {

	@JsonProperty("combinationRevisionId")
	private Integer id;
	@JsonProperty("revision")
	private Integer revision;
	@JsonProperty("createdBy")
	private String createdBy;
	@JsonProperty("createdOn")
	private String createdOn;
	@JsonProperty("reason")
	private String reason;
	@JsonProperty("active")
	private boolean active;

}
