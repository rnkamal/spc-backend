package com.aptar.spc.DTO.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MachineWithSpcResponse implements Serializable {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("combinationRevisionId")
	private Integer combination_revision_id;
	@JsonProperty("prodOrd")
	private String po;
	@JsonProperty("machine")
	private String machine_name;
	@JsonProperty("product")
	private String product;
	@JsonProperty("status")
	private String spc_status;
}
