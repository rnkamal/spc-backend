package com.aptar.spc.DTO.response;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CombinationResponse {

	@JsonProperty("attributes")
	private List<String> attributes;
	@JsonProperty("combinationResponse")
	private List<Map<Object, Object>> combinationResponse;

}
