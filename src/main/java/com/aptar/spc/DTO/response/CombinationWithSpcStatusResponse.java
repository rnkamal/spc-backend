package com.aptar.spc.DTO.response;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CombinationWithSpcStatusResponse {

	@JsonProperty("attributes")
	private List<String> attributes;
	@JsonProperty("combinationResponse")
	private List<Map<?, ?>> combinationResponse;
	@JsonProperty("prodOrd")
	private String po;
	@JsonProperty("status")
	private String spc_status;

}
