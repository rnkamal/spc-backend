package com.aptar.spc.DTO.response;

import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ControlChartResponse {

	@JsonProperty("attributeI")
	private List<Attribute> attribute_I;
	@JsonProperty("attributeMR")
	private List<Attribute> attribute_MR;
	@JsonProperty("attributeXBAR")
	private List<Attribute> attribute_XBar;
	@JsonProperty("attributeS")
	private List<Attribute> attribute_S;
	@JsonProperty("attributeR")
	private List<Attribute> attribute_R;

	private HashMap<Integer, Object> subGroups;

	@JsonProperty("upperControlLimitI")
	private List<Double> ucl_I;
	@JsonProperty("meanI")
	private double mean_I;
	@JsonProperty("lowerControLimitI")
	private List<Double> lcl_I;

	@JsonProperty("upperControlLimitMR")
	private List<Double> ucl_MR;
	@JsonProperty("meanMR")
	private double mean_MR;
	@JsonProperty("lowerControLimitMR")
	private List<Double> lcl_MR;

	@JsonProperty("upperControlLimitXBAR")
	private List<Double> ucl_XBar;
	@JsonProperty("meanXBAR")
	private double mean_XBar;
	@JsonProperty("lowerControLimitXBAR")
	private List<Double> lcl_XBar;

	@JsonProperty("upperControlLimitS")
	private List<Double> ucl_S;
	@JsonProperty("meanS")
	private double mean_S;
	@JsonProperty("lowerControLimitS")
	private List<Double> lcl_S;

	@JsonProperty("upperControlLimitR")
	private List<Double> ucl_R;
	@JsonProperty("meanR")
	private double mean_R;
	@JsonProperty("lowerControLimitR")
	private List<Double> lcl_R;

	@JsonProperty("excelId")
	private Integer excelId;

}
