package com.aptar.spc.DTO.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class AlertResponse {

	@JsonProperty("Id")
	private Integer id;
	@JsonProperty("Machine")
	private String machine;
	@JsonProperty("Site")
	private String site;
	@JsonProperty("ProductionOrder")
	private String productionOrder;
	@JsonProperty("TagID")
	private String tagID;
	@JsonProperty("Graph")
	private String graph;
	@JsonProperty("Path")
	private String path;
	@JsonProperty("AlertValue")
	private String alertValue;
	@JsonProperty("ViolatedRules")
	private String violatedRule;
	@JsonProperty("Timestamp")
	private String timestamp;
}
