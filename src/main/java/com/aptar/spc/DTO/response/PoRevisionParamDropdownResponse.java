package com.aptar.spc.DTO.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PoRevisionParamDropdownResponse implements Serializable {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("description")
	private String parameter;
}
