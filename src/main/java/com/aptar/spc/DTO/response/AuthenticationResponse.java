package com.aptar.spc.DTO.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Object to return as body in JWT Authentication.
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationResponse {

	private String idToken;

	@JsonProperty("id_token")
	public String getIdToken() {
		return idToken;
	}
}
