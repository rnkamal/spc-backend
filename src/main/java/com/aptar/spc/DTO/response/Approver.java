package com.aptar.spc.DTO.response;

import java.sql.Timestamp;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Approver {

	private Integer approverID;
	private Timestamp approverOn;
	private boolean approved;
}
