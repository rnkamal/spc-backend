package com.aptar.spc.DTO.response;

import com.aptar.spc.entity.common.RevisionParamDetails;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class RevisionParamResponse {

	@JsonProperty("revisionParamId")
	private Integer id;
	@JsonProperty("parameterId")
	private Integer parameterId;
	@JsonProperty("parameterName")
	private String parameterName;
	@JsonProperty("spc")
	private boolean spc;
	@JsonProperty("combinationRevisionId")
	private Integer combinationRevisionId;
	@JsonProperty("controlChartTypeName")
	private String controlChartTypeName;
	@JsonProperty("graphName")
	private String graphName;
	@JsonProperty("revisionParamDetails")
	private RevisionParamDetails revisionParamDetails;
}
