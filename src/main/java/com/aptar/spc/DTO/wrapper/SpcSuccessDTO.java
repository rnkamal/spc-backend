package com.aptar.spc.DTO.wrapper;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.ToString;

@SuppressWarnings("serial")
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SpcSuccessDTO<T> implements java.io.Serializable {
	private T body;
	private int length = 1;
	private String message = null;

	public SpcSuccessDTO(T body, int length, String message) {
		this.body = body;
		this.length = length;
		this.message = message;
		if (length == 0) {
			if (this.body instanceof List) {
				this.length = ((List) this.body).size();
			}

			if (this.body instanceof Map) {
				this.length = ((Map) this.body).size();
			}
		}

	}

	public SpcSuccessDTO(T body, String message) {
		this.body = body;
		this.message = message;

		if (this.body instanceof List) {
			this.length = ((List) this.body).size();
		}

		if (this.body instanceof Map) {
			this.length = ((Map) this.body).size();
		}
	}

	public SpcSuccessDTO(T body, Integer length) {
		this.body = body;
		this.length = length;
	}

	public SpcSuccessDTO(T body) {
		this.body = body;
		if (this.body instanceof List) {
			this.length = ((List) this.body).size();
		}

		if (this.body instanceof Map) {
			this.length = ((Map) this.body).size();
		}
	}
}
