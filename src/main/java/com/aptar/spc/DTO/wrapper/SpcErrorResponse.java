package com.aptar.spc.DTO.wrapper;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SpcErrorResponse<T> {
	private SpcErrorDTO<T> error;

	public SpcErrorResponse(HttpStatus code, String message) {
		error = new SpcErrorDTO<T>(code, message);
	}

}
