package com.aptar.spc.DTO.wrapper;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SpcSuccessResponse<T> {
	private SpcSuccessDTO<T> response;

	public SpcSuccessResponse(T object) {
		this.response = new SpcSuccessDTO<>(object);
	}

	public SpcSuccessResponse(T object, String message) {
		this.response = new SpcSuccessDTO<>(object, message);
	}

	public SpcSuccessResponse(T object, Integer length, String message) {
		this.response = new SpcSuccessDTO<>(object, length, message);
	}

	public SpcSuccessResponse(T object, Integer length) {
		this.response = new SpcSuccessDTO<>(object, length);
	}

}