package com.aptar.spc.DTO.wrapper;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SpcErrorDTO<T> implements java.io.Serializable {
//	@JsonInclude(JsonInclude.Include.NON_NULL)
//	private T body;
	private HttpStatus code;
	private String message;

}