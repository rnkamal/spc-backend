package com.aptar.spc.DTO.historian.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BatchResponse {

//	@JsonProperty("SubAttributes")
//	private Elements subAttributes;
//	@JsonProperty("Attributes")
//	private Elements attributes;
	@JsonProperty("InjectionUnitAttributes")
	private Elements injectionUnitAttributes;
	@JsonProperty("InjectionUnitElements")
	private Elements injectionUnitElements;
	@JsonProperty("MachineElements")
	private Elements machineElements;
	@JsonProperty("Machine")
	private Elements machine;
	@JsonProperty("PESElements")
	private Elements pESElements;
	@JsonProperty("ModuleDBElements")
	private Elements moduleDBElements;
	@JsonProperty("Elements")
	private Elements elements;
	@JsonProperty("Databases")
	private Elements databases;
	@JsonProperty("AssertServerWebID")
	private AssertServerWebID assertServerWebID;

}
