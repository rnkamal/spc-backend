package com.aptar.spc.DTO.historian.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Elements {

	@JsonProperty("Status")
	private String status;
	@JsonProperty("Headers")
	private Headers headers;
	@JsonProperty("Content")
	private ContentWithHeader contentWithHeader;
}
