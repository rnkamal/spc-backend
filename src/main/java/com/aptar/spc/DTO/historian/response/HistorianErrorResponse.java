package com.aptar.spc.DTO.historian.response;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.aptar.spc.exception.HistorianRestTemplateException;

public class HistorianErrorResponse {

	private String timestamp;

	private int status;

	private String error;

	private String message;

	private String path;

	public HistorianErrorResponse(HistorianRestTemplateException ex, String path) {
		this.timestamp = DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(LocalDateTime.now());
		this.status = ex.getStatusCode().value();
		this.error = ex.getStatusCode().getReasonPhrase();
		this.message = ex.getError();
		this.path = path;
	}

}
