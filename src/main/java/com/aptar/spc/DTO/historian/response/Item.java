package com.aptar.spc.DTO.historian.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Item {

	@JsonProperty("WebId")
	private String webId;
	@JsonProperty("Name")
	private String name;
	@JsonProperty("Timestamp")
	private String timestamp;
	@JsonProperty("Value")
	private String value;
}
