package com.aptar.spc.DTO.historian.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SubAttribute {

	@JsonProperty("Timestamp")
	private String timestamp;
	@JsonProperty("UnitsAbbreviation")
	private String unitsAbbreviation;
	@JsonProperty("Value")
	private Double value;
}
