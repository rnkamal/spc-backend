package com.aptar.spc.DTO.historian.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MachineParamterRequest {

	@JsonProperty("MachineName")
	private String machineName;
	@JsonProperty("MachineParameterTag")
	private String machineTag;
	@JsonProperty("Chart")
	private String chart;
	@JsonProperty("Paramter")
	private String paramter;
	@JsonProperty("Value")
	private Double value;
}
