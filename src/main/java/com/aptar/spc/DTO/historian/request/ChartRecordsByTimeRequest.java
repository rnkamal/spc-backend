package com.aptar.spc.DTO.historian.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChartRecordsByTimeRequest {

	@JsonProperty("MachineName")
	private String machineName;
	@JsonProperty("MachineParamaterTag")
	private String machineParamaterTag;
	@JsonProperty("Chart")
	private String chart;
	@JsonProperty("Time")
	private String time;
	@JsonProperty("MaxCount")
	private Integer maxCount;
}
