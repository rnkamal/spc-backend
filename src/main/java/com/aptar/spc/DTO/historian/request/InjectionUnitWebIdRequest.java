package com.aptar.spc.DTO.historian.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InjectionUnitWebIdRequest {

	@JsonProperty("MachineName")
	private String machineName;
	@JsonProperty("MachineParameterTag")
	private String machineTag;
}
