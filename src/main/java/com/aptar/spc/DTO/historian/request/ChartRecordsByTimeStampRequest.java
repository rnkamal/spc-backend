package com.aptar.spc.DTO.historian.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChartRecordsByTimeStampRequest {

	@JsonProperty("MachineName")
	private String machineName;
	@JsonProperty("MachineParamaterTag")
	private String machineParamaterTag;
	@JsonProperty("Chart")
	private String chart;
	@JsonProperty("StartTimeStamp")
	private String startTimeStamp;
	@JsonProperty("MaxCount")
	private Integer maxCount;
}
