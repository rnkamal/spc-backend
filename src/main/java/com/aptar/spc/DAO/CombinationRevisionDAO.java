package com.aptar.spc.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.Combination;
import com.aptar.spc.entity.CombinationRevision;
import com.aptar.spc.entity.CombinationRevisionParameterRef;

@Repository
public interface CombinationRevisionDAO extends JpaRepository<CombinationRevision, Integer> {
	List<CombinationRevision> findByCombination(Combination combination);

	CombinationRevision findByCombinationIdAndActive(Integer combinationId, boolean active);

	@Query(value = "select s.id from combination_revisions s where s.combination_id= ?1", nativeQuery = true)
	List<Integer> getCombinationRevisionId(final Integer combinationId);

	CombinationRevision findByCombinationRevisionParameterRefs(
			CombinationRevisionParameterRef combinationRevisionParameterRefs);
}
