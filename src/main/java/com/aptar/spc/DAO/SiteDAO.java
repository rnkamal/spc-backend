package com.aptar.spc.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.Site;
import com.aptar.spc.entity.UserRole;

@Repository
public interface SiteDAO extends JpaRepository<Site, Integer> {
	List<Site> findByUserRolesIn(List<UserRole> userRoles);
}
