package com.aptar.spc.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.CombinationAttribute;
import com.aptar.spc.entity.CombinationAttributeValue;

@Repository
public interface CombinationAttributeDAO extends JpaRepository<CombinationAttribute, Integer> {
	List<CombinationAttribute> findByCombinationAttributeValuesIn(
			List<CombinationAttributeValue> combinationAttributeValues);
}
