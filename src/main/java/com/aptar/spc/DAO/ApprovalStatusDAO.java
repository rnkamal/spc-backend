package com.aptar.spc.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.ApprovalStatus;

@Repository
public interface ApprovalStatusDAO extends JpaRepository<ApprovalStatus, Integer> {
}
