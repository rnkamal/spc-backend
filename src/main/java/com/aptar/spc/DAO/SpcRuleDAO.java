package com.aptar.spc.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.SpcRule;

@Repository
public interface SpcRuleDAO extends JpaRepository<SpcRule, Integer> {

	@Query(value = "select s.rule from spc_rules s where s.id In ?1", nativeQuery = true)
	List<String> getSpcRuleName(final Integer[] ids);
}
