package com.aptar.spc.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.CombinationRevisionParameterRef;

@Repository
public interface CombinationRevisionParameterRefDAO extends JpaRepository<CombinationRevisionParameterRef, Integer> {

	List<CombinationRevisionParameterRef> findAllByCombinationRevisionIdIn(List<Integer> combinationRevisionIds);

	List<CombinationRevisionParameterRef> findAllByCombinationRevisionIdInAndSpc(List<Integer> combinationRevisionIds,
			boolean spc);

	List<CombinationRevisionParameterRef> findAllByIdIn(List<Integer> RevisionIds);
}
