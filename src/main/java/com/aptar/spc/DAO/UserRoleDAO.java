package com.aptar.spc.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.User;
import com.aptar.spc.entity.UserRole;

@Repository
public interface UserRoleDAO extends JpaRepository<UserRole, Integer> {
	List<UserRole> findByUser(User user);
}
