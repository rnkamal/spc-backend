package com.aptar.spc.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.RolePrivilege;

@Repository
public interface RolePrivilegeDAO extends JpaRepository<RolePrivilege, Integer> {

}
