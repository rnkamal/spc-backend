package com.aptar.spc.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.ControlChartType;
import com.aptar.spc.entity.Graph;

@Repository
public interface GraphDAO extends JpaRepository<Graph, Integer> {

	List<Graph> findByControlChartType(ControlChartType controlChartType);

	List<Graph> findByControlChartTypeId(Integer controlChartTypeId);
}
