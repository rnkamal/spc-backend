package com.aptar.spc.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.PoParameterApprovalStatus;

@Repository
public interface PoParameterApprovalStatusDAO extends JpaRepository<PoParameterApprovalStatus, Integer> {
}
