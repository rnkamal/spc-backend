package com.aptar.spc.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.FunctionalLoc;
import com.aptar.spc.entity.Machine;

@Repository
public interface FunctionalLocDAO extends JpaRepository<FunctionalLoc, Integer> {
	List<FunctionalLoc> findByMachinesIn(List<Machine> machines);
}
