package com.aptar.spc.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.Privilege;

@Repository
public interface PrivilegeDAO extends JpaRepository<Privilege, Integer> {

}
