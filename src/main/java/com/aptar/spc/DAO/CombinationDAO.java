package com.aptar.spc.DAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.Combination;
import com.aptar.spc.entity.CombinationRevision;
import com.aptar.spc.entity.Machine;

@Repository
public interface CombinationDAO extends JpaRepository<Combination, Integer> {

	List<Combination> findByMachineIn(List<Machine> machine);

	Combination findByCombinationRevisions(Optional<CombinationRevision> combinationRevision);
}
