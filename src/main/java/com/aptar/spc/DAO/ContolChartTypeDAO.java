package com.aptar.spc.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.ControlChartType;

@Repository
public interface ContolChartTypeDAO extends JpaRepository<ControlChartType, Integer> {

}
