package com.aptar.spc.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.CombinationAttribute;
import com.aptar.spc.entity.CombinationAttributeTemplate;

@Repository
public interface CombinationAttributeTemplateDAO extends JpaRepository<CombinationAttributeTemplate, Integer> {
	List<CombinationAttributeTemplate> findByCombinationAttributeIn(List<CombinationAttribute> combinationAttribute);
}
