package com.aptar.spc.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.Combination;
import com.aptar.spc.entity.ProductionOrder;

@Repository
public interface ProductionOrderDAO extends JpaRepository<ProductionOrder, Integer> {
	List<ProductionOrder> findByCombinationsIn(List<Combination> combinations);

	ProductionOrder findByProductionOrder(String productionOrder);
}
