package com.aptar.spc.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.Combination;
import com.aptar.spc.entity.Department;
import com.aptar.spc.entity.FunctionalLoc;
import com.aptar.spc.entity.Machine;

@Repository
public interface MachineDAO extends JpaRepository<Machine, Integer> {
	List<Machine> findByFunctionalLoc(FunctionalLoc functionalLoc);

	List<Machine> findByDepartmentIn(List<Department> department);

	List<Machine> findByCombinationsIn(List<Combination> combinations);
}
