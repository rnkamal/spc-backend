package com.aptar.spc.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.Combination;
import com.aptar.spc.entity.CombinationAttributeValue;

@Repository
public interface CombinationAttributeValueDAO extends JpaRepository<CombinationAttributeValue, Integer> {
	List<CombinationAttributeValue> findByCombinationIn(List<Combination> combination);
}
