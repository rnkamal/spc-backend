package com.aptar.spc.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.User;

@Repository
public interface UsersDAO extends JpaRepository<User, Integer> {

	List<User> findByFirstname(String firstname);

}
