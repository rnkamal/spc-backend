package com.aptar.spc.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.SpcAlert;

@Repository
public interface SpcAlertDAO extends JpaRepository<SpcAlert, Integer> {

}
