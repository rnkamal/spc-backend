package com.aptar.spc.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.UploadExcel;

@Repository
public interface UploadExcelDAO extends JpaRepository<UploadExcel, Integer> {

}
