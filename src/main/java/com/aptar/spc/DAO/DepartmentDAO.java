package com.aptar.spc.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.Department;
import com.aptar.spc.entity.Site;
import com.aptar.spc.entity.UserRole;

@Repository
public interface DepartmentDAO extends JpaRepository<Department, Integer> {
	List<Department> findBySite(Site site);

	List<Department> findByUserRolesIn(List<UserRole> userRoles);

	List<Department> findBySiteIn(List<Site> site);
}
