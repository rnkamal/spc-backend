package com.aptar.spc.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.MachineParameter;

@Repository
public interface MachineParameterDAO extends JpaRepository<MachineParameter, Integer> {

	MachineParameter findByTag(String tag);
}
