package com.aptar.spc.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.PoCombinationRevisionParameterRef;
import com.aptar.spc.entity.ProductionOrder;

@Repository
public interface PoCombinationRevisionParameterRefDAO
		extends JpaRepository<PoCombinationRevisionParameterRef, Integer> {

	List<PoCombinationRevisionParameterRef> findAllByCombinationRevisionIdIn(List<Integer> CombinationRevisionId);

	Integer countByCombinationRevisionIdAndSpcAndActive(Integer combinationRevisionId, boolean spc, boolean active);

	Integer countByCombinationRevisionIdAndSpcAndActiveAndAlertStatus(Integer combinationRevisionId, boolean spc,
			boolean active, boolean alertStatus);

	@Modifying(clearAutomatically = true)
	@Query(value = "UPDATE po_combination_revision_parameter_ref c SET spc = ?1 WHERE c.combination_revision_id = ?2", nativeQuery = true)
	Integer updateSpc(final boolean spc, final Integer combinationRevisionIds);

	List<PoCombinationRevisionParameterRef> findAllByCombinationRevisionIdInAndSpc(List<Integer> combinationRevisionIds,
			boolean spc);

	List<PoCombinationRevisionParameterRef> findAllByIdIn(List<Integer> RevisionIds);

	List<PoCombinationRevisionParameterRef> findIdAndPrameterIdByCombinationRevisionIdInAndSpc(
			List<Integer> combinationRevisionIds, boolean spc);

	PoCombinationRevisionParameterRef findByParameterIdAndProductionOrder(Integer ParameterId, ProductionOrder PoId);
}
