package com.aptar.spc.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aptar.spc.entity.MachineGroup;

@Repository
public interface MachineGroupDAO extends JpaRepository<MachineGroup, Integer> {

}
