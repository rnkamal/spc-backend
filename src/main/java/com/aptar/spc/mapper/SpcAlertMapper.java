package com.aptar.spc.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aptar.spc.DTO.request.AlertFromHistorianRequest;
import com.aptar.spc.DTO.response.AlertResponse;
import com.aptar.spc.entity.PoCombinationRevisionParameterRef;
import com.aptar.spc.entity.SpcAlert;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SpcAlertMapper {

	@Autowired
	ModelMapper modelMapper;

	public SpcAlert toSpcAlertEntity(AlertFromHistorianRequest alertFromHistorianRequest,
			PoCombinationRevisionParameterRef poCombinationRevisionParameterRef) {
		log.info("-->SpcAlertMapper.toSpcAlertEntity");
		SpcAlert spcAlert = modelMapper.map(alertFromHistorianRequest, SpcAlert.class);
		spcAlert.setPoCombinationRevisionParameterRefInAlert(poCombinationRevisionParameterRef);
		return spcAlert;
	}

	public AlertResponse toSpcAlertResponse(SpcAlert spcAlert) {
		log.info("-->SpcAlertMapper.toSpcAlertResponse");
		AlertResponse alertResponse = modelMapper.map(spcAlert, AlertResponse.class);
		return alertResponse;
	}
}
