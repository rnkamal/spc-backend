package com.aptar.spc.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aptar.spc.DTO.response.SiteResponse;
import com.aptar.spc.entity.Site;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SiteMapper {

	@Autowired
	ModelMapper modelMapper;

	public SiteResponse toSiteResponse(Site site) {
		log.info("-->SiteMapper.toSiteResponse");
		SiteResponse siteResponse = modelMapper.map(site, SiteResponse.class);
		return siteResponse;
	}
}
