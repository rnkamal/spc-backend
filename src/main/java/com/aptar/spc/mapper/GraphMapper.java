package com.aptar.spc.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aptar.spc.DTO.response.GraphResponse;
import com.aptar.spc.entity.Graph;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class GraphMapper {

	@Autowired
	ModelMapper modelMapper;

	public GraphResponse toGraphResponse(Graph graph) {
		log.info("-->GraphMapper.toGraphResponse");
		return modelMapper.map(graph, GraphResponse.class);
	}
}
