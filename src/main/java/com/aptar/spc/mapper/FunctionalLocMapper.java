package com.aptar.spc.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aptar.spc.DTO.response.FunctionalLocResponse;
import com.aptar.spc.entity.FunctionalLoc;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class FunctionalLocMapper {

	@Autowired
	ModelMapper modelMapper;

	public FunctionalLocResponse toFunctionLocResponse(FunctionalLoc functionalLoc) {
		log.info("-->FunctionalLocMapper.toFunctionLocResponse");
		FunctionalLocResponse functionalLocResponse = modelMapper.map(functionalLoc, FunctionalLocResponse.class);
		return functionalLocResponse;
	}
}
