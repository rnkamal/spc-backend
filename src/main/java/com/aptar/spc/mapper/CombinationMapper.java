package com.aptar.spc.mapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aptar.spc.DTO.response.CombinationResponse;
import com.aptar.spc.constants.Constants;
import com.aptar.spc.entity.Combination;
import com.aptar.spc.entity.CombinationAttribute;
import com.aptar.spc.entity.CombinationRevision;
import com.aptar.spc.exception.MapperException;
import com.aptar.spc.service.DAOservice.CombinationRevisionService;
import com.aptar.spc.service.DAOservice.PoRevisionParamService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CombinationMapper {

	@Autowired
	ModelMapper modelMapper;

	@Autowired
	CombinationRevisionService combinationRevisionService;

	@Autowired
	PoRevisionParamService poRevisionParamService;

	public CombinationResponse toCombinationResponse(Combination combination) {
		try {
			log.info("--> CombinationMapper.toCombinationResponse()");
			return toCombinationResponse(Arrays.asList(combination));
		} catch (Exception e) {
			log.error("Error occurs in CombinationMapper class --> toCombinationResponse method",
					e.getLocalizedMessage());
			throw new MapperException(e);
		}
	}

	public CombinationResponse toCombinationWithSpcStatusResponse(Combination combination) {
		try {
			log.info("--> CombinationMapper.toCombinationResponse()");
			CombinationResponse combinationResponse = toCombinationResponse(Arrays.asList(combination));
			combinationResponse.getAttributes().add(Constants.SPC_STATUS_HEADER);
			combinationResponse.getAttributes().add(Constants.PROD_ORD_HEADER);
			Map<Object, Object> attributeValues = new HashMap<>();
			attributeValues.put(Constants.PROD_ORD_HEADER, combination.getProductionOrder().getId());
			attributeValues.put(Constants.REVISION_PARAM_ID,
					combinationRevisionService.getActiveCombinationRevision(combination.getId()).getId());
			CombinationRevision combinationRevision = combinationRevisionService
					.getActiveCombinationRevision(combination.getId());
			String status = Constants.SPC_STATUS_INACTIVE;
			if (combinationRevision != null)
				status = poRevisionParamService.getSpcStatusByRevisionId(combinationRevision.getId());

			attributeValues.put(Constants.SPC_STATUS_HEADER, status);
			combinationResponse.getCombinationResponse().forEach(m -> {
				m.putAll(attributeValues);
			});
			return combinationResponse;
		} catch (Exception e) {
			log.error("Error occurs in CombinationMapper class --> toCombinationResponse method",
					e.getLocalizedMessage());
			throw new MapperException(e);
		}
	}

	@SuppressWarnings("null")
	public CombinationResponse toCombinationResponse(List<Combination> combination) {
		try {
			log.info("--> CombinationMapper.toCombinationResponse()");
			List<Map<Object, Object>> combinationResponses = new ArrayList<>();
			List<String> combinationAttributesHeader = new ArrayList<>();
			combination.forEach(c -> {
				combinationResponses.add(getcombinationAttributeValueMap(c, combinationAttributesHeader));
			});
			if (!combinationAttributesHeader.isEmpty())
				combinationAttributesHeader.addAll(Constants.COMBINATION_HEADER_CONSTANTS);
			return CombinationResponse.builder().attributes(combinationAttributesHeader)
					.combinationResponse(combinationResponses).build();
		} catch (Exception e) {
			log.error("Error occurs in CombinationMapper class --> toCombinationResponse method",
					e.getLocalizedMessage());
			throw new MapperException(e);
		}
	}

	private Map<Object, Object> getcombinationAttributeValueMap(Combination c,
			List<String> combinationAttributesHeader) {
		try {
			log.info("--> CombinationMapper.getcombinationAttributeValueMap()");
			Map<Object, Object> attributeValues = new HashMap<>();
			c.getCombinationAttributeValues().forEach(cdv -> {
				CombinationAttribute combinationAttribute = cdv.getCombinationAttribute();
				if (combinationAttributesHeader.isEmpty())
					combinationAttributesHeader.add(combinationAttribute.getName());
				else {
					if (!(combinationAttributesHeader.contains(combinationAttribute.getName()))) {
						combinationAttributesHeader.add(combinationAttribute.getName());
					}
				}
				attributeValues.put(combinationAttribute.getName(), cdv.getValue());
			});
			attributeValues.put("machine", c.getMachine().getName());
			attributeValues.put("combinationId", c.getId());
			return attributeValues;
		} catch (Exception e) {
			log.error("Error occurs in CombinationMapper class --> getcombinationAttributeValueMap method",
					e.getLocalizedMessage());
			throw new MapperException(e);
		}
	}

}
