package com.aptar.spc.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aptar.spc.DTO.response.ControlChartTypeResponse;
import com.aptar.spc.entity.ControlChartType;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ControlChartTypeMapper {

	@Autowired
	ModelMapper modelMapper;

	public ControlChartTypeResponse toControlChartTypeResponse(ControlChartType controlChartType) {
		log.info("-->ControlChartTypeMapper.toControlChartTypeResponse");
		return modelMapper.map(controlChartType, ControlChartTypeResponse.class);
	}
}
