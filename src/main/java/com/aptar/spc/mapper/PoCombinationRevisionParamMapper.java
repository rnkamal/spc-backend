package com.aptar.spc.mapper;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aptar.spc.DTO.response.GraphDetailsResponse;
import com.aptar.spc.DTO.response.PoRevisionParamDropdownResponse;
import com.aptar.spc.DTO.response.PoRevisionParamResponse;
import com.aptar.spc.DTO.response.RevisionParamDetailsResponse;
import com.aptar.spc.entity.PoCombinationRevisionParameterRef;
import com.aptar.spc.entity.common.GraphDetails;
import com.aptar.spc.entity.common.RevisionParamDetails;
import com.aptar.spc.exception.MapperException;
import com.aptar.spc.service.DAOservice.ContolChartTypeService;
import com.aptar.spc.service.DAOservice.GraphService;
import com.aptar.spc.service.DAOservice.MachineParameterService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class PoCombinationRevisionParamMapper {

	@Autowired
	ModelMapper modelMapper;

	@Autowired
	GraphService graphService;

	@Autowired
	ContolChartTypeService contolChartTypeService;

	@Autowired
	MachineParameterService machineParameterService;

	public List<PoRevisionParamResponse> toPoRevisionParamResponseList(
			List<PoCombinationRevisionParameterRef> poRevisionParams) throws MapperException {
		log.info("--> PoCombinationRevisionParamMapper.toPoRevisionParamResponseList()");
		List<PoRevisionParamResponse> poRevisionParamResponseList = new ArrayList<>();

		poRevisionParams.forEach(p -> {
			poRevisionParamResponseList.add(toPoRevisionParamResponse(p));
		});
		return poRevisionParamResponseList;
	}

	public List<PoRevisionParamDropdownResponse> toPoRevisionParamDropdownResponseResponseList(
			List<PoCombinationRevisionParameterRef> poRevisionParams) throws MapperException {
		log.info("--> PoCombinationRevisionParamMapper.toPoRevisionParamDropdownResponseResponseList()");
		List<PoRevisionParamDropdownResponse> poRevisionParamDropdownResponseList = new ArrayList<>();

		poRevisionParams.forEach(p -> {
			poRevisionParamDropdownResponseList.add(toPoRevisionParamDropdownResponse(p));
		});
		return poRevisionParamDropdownResponseList;
	}

	public PoRevisionParamResponse toPoRevisionParamResponse(PoCombinationRevisionParameterRef poRevisionParams)
			throws MapperException {
		log.info("--> PoCombinationRevisionParamMapper.toPoRevisionParamResponse()");

		return PoRevisionParamResponse.builder().id(poRevisionParams.getId())
				.parameterId(poRevisionParams.getParameterId())
				.combinationRevisionId(poRevisionParams.getCombinationRevisionId()).spc(poRevisionParams.getSpc())
				.parameterName(
						machineParameterService.getMachineParameterById(poRevisionParams.getParameterId()).getName())
				.parameterTag(
						machineParameterService.getMachineParameterById(poRevisionParams.getParameterId()).getTag())
				.controlChartTypeName(contolChartTypeService
						.getContolChartTypeNameById(poRevisionParams.getRevisionParamDetails().getControlChartTypeId()))
				.graphName(graphService.getGraphName(poRevisionParams.getRevisionParamDetails().getGraphId()))
				.revisionParamDetails(toRevisionParamDetailsResponse(poRevisionParams.getRevisionParamDetails()))
				.build();
	}

	public RevisionParamDetailsResponse toRevisionParamDetailsResponse(RevisionParamDetails revisionParamDetails) {
		log.info("-->PoCombinationRevisionParamMapper.toRevisionParamDetailsResponse");
		RevisionParamDetailsResponse convertedRevisionParamDetailsResponse = modelMapper.map(revisionParamDetails,
				RevisionParamDetailsResponse.class);
		List<GraphDetailsResponse> graphDetailsResponseList = new ArrayList<>();
		revisionParamDetails.getGraphDetails().stream().forEach(rpd -> {
			graphDetailsResponseList.add(toGraphDetailsResponse(rpd));
		});
		convertedRevisionParamDetailsResponse.setGraphDetails(graphDetailsResponseList);
		return convertedRevisionParamDetailsResponse;
	}

	public GraphDetailsResponse toGraphDetailsResponse(GraphDetails graphDetails) {
		log.info("-->PoCombinationRevisionParamMapper.toGraphDetailsResponse");
		GraphDetailsResponse convertedGraphDetails = modelMapper.map(graphDetails, GraphDetailsResponse.class);
		convertedGraphDetails.setGraphName(graphService.getGraphName(convertedGraphDetails.getGraphId()));
		return convertedGraphDetails;
	}

	public PoRevisionParamDropdownResponse toPoRevisionParamDropdownResponse(
			PoCombinationRevisionParameterRef poRevisionParams) throws MapperException {
		log.info("--> PoCombinationRevisionParamMapper.toPoRevisionParamDropdownResponseResponse()");

		return PoRevisionParamDropdownResponse.builder().id(poRevisionParams.getId())
				.parameter(machineParameterService.getMachineParameterById(poRevisionParams.getId()).getName()).build();
	}
}
