package com.aptar.spc.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aptar.spc.DTO.historian.response.ItemList;
import com.aptar.spc.DTO.historian.response.ItemListChartResponse;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class HistorianMapper {

	@Autowired
	ModelMapper modelMapper;

	public ItemListChartResponse toItemListChartResponseResponse(ItemList itemList) {
		log.info("-->HistorianMapper.toItemListChartResponseResponse");
		ItemListChartResponse itemListChartResponse = modelMapper.map(itemList, ItemListChartResponse.class);
		return itemListChartResponse;
	}
}
