package com.aptar.spc.mapper;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aptar.spc.DAO.CombinationRevisionDAO;
import com.aptar.spc.DTO.request.CombinationRevisionRequest;
import com.aptar.spc.DTO.request.RevisionParamRequest;
import com.aptar.spc.DTO.response.CombinationRevisionParamResponse;
import com.aptar.spc.DTO.response.CombinationRevisionResponse;
import com.aptar.spc.DTO.response.RevisionParamResponse;
import com.aptar.spc.entity.CombinationRevision;
import com.aptar.spc.entity.CombinationRevisionParameterRef;
import com.aptar.spc.exception.MapperException;
import com.aptar.spc.service.DAOservice.ContolChartTypeService;
import com.aptar.spc.service.DAOservice.GraphService;
import com.aptar.spc.service.DAOservice.MachineParameterService;
import com.aptar.spc.service.DAOservice.SpcRuleService;
import com.aptar.spc.service.DAOservice.UserService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CombinationRevisionParamMapper {

	@Autowired
	ModelMapper modelMapper;

	@Autowired
	UserService userService;

	@Autowired
	ContolChartTypeService contolChartTypeService;

	@Autowired
	GraphService graphService;

	@Autowired
	SpcRuleService spcRuleService;

	@Autowired
	CombinationRevisionDAO combinationRevisionDAO;

	@Autowired
	MachineParameterService machineParameterService;

	public CombinationRevision toCombinationRevision(CombinationRevisionRequest combinationRevisionRequest) {
		CombinationRevision combinationRevision = modelMapper.map(combinationRevisionRequest,
				CombinationRevision.class);
		return combinationRevision;
	}

	public CombinationRevisionParameterRef toCombinationRevisionParameterRef(
			RevisionParamRequest revisionParamRequest) {
		CombinationRevisionParameterRef combinationRevisionParameterRef = modelMapper.map(revisionParamRequest,
				CombinationRevisionParameterRef.class);
		combinationRevisionParameterRef.setCombinationRevision(
				combinationRevisionDAO.findById(revisionParamRequest.getCombinationRevisionId()).get());
		return combinationRevisionParameterRef;
	}

	@SuppressWarnings("null")
	public CombinationRevisionParamResponse toCombinationRevisionParamResponse(CombinationRevision combinationRevision,
			List<RevisionParamResponse> revisionParams) throws MapperException {
		log.info("--> CombinationRevisionParamMapper.toCombinationRevisionParamResponse()");
		CombinationRevisionParamResponse combinationRevisionParamResponse = modelMapper
				.map(toCombinationRevisionResponse(combinationRevision), CombinationRevisionParamResponse.class);

		combinationRevisionParamResponse.setRevisionParam(revisionParams);
		return combinationRevisionParamResponse;
	}

	public CombinationRevisionResponse toCombinationRevisionResponse(CombinationRevision combinationRevision) {
		try {
			log.info("--> CombinationRevisionParamMapper.toCombinationRevisionResponse()");
			return CombinationRevisionResponse.builder().id(combinationRevision.getId())
					.active(combinationRevision.getActive()).reason(combinationRevision.getReason())
					.revision(combinationRevision.getRevison())
					.createdBy(userService.getUserByUserName(combinationRevision.getCreatedby()).getFirstname())
					.build();
		} catch (Exception e) {
			log.error("Error in CombinationRevisionResponse class toCombinationRevisionResponse method : "
					+ e.getMessage());
			throw new MapperException(e);
		}
	}

	public List<CombinationRevisionResponse> toCombinationRevisionResponseList(
			List<CombinationRevision> combinationRevisions) {
		try {
			log.info("--> CombinationRevisionParamMapper.toCombinationRevisionResponseList()");
			List<CombinationRevisionResponse> combinationRevisionResponseList = new ArrayList<>();
			combinationRevisions.forEach(cr -> {
				combinationRevisionResponseList.add(toCombinationRevisionResponse(cr));
			});
			return combinationRevisionResponseList;
		} catch (Exception e) {
			log.error("Error in CombinationRevisionResponse class toCombinationRevisionResponseList method");
			throw new MapperException(e);
		}
	}

	public List<RevisionParamResponse> toRevisionParamResponseList(List<CombinationRevisionParameterRef> revisionParams)
			throws MapperException {
		log.info("--> CombinationRevisionParamMapper.toRevisionParamResponseList()");
		List<RevisionParamResponse> revisionParamResponseList = new ArrayList<>();

		revisionParams.forEach(p -> {
			revisionParamResponseList.add(toRevisionParamResponse(p));
		});
		return revisionParamResponseList;
	}

	public RevisionParamResponse toRevisionParamResponse(CombinationRevisionParameterRef revisionParams)
			throws MapperException {
		log.info("--> CombinationRevisionParamMapper.toRevisionParamResponse()");

		return RevisionParamResponse.builder().id(revisionParams.getId()).parameterId(revisionParams.getParameterId())
				.combinationRevisionId(revisionParams.getCombinationRevision().getId()).spc(revisionParams.getSpc())
				.parameterName(machineParameterService.getMachineParameterById(revisionParams.getId()).getName())
				.controlChartTypeName(contolChartTypeService
						.getContolChartTypeNameById(revisionParams.getRevisionParamDetails().getControlChartTypeId()))
				.graphName(graphService.getGraphName(revisionParams.getRevisionParamDetails().getGraphId()))
				.revisionParamDetails(revisionParams.getRevisionParamDetails()).build();
	}
}
