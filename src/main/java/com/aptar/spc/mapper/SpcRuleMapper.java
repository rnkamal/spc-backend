package com.aptar.spc.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aptar.spc.DTO.response.SpcRuleResponse;
import com.aptar.spc.entity.SpcRule;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SpcRuleMapper {

	@Autowired
	ModelMapper modelMapper;

	public SpcRuleResponse toSpcRuleResponse(SpcRule spcRule) {
		log.info("-->SpcRuleMapper.toSpcRuleResponse");
		SpcRuleResponse spcRuleResponse = modelMapper.map(spcRule, SpcRuleResponse.class);
		return spcRuleResponse;
	}
}
