package com.aptar.spc.service.controlchartservice;

import java.util.Arrays;
import java.util.stream.IntStream;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Service;

import com.aptar.spc.DTO.response.ControlChartResponse;
import com.aptar.spc.constants.ControlChartConstants;
import com.aptar.spc.exception.ChartCalulationException;
import com.aptar.spc.service.interfaces.ControlChartCalculations;
import com.aptar.spc.util.ControlChartUtility;

import lombok.extern.slf4j.Slf4j;

@Service("IMRChartCalculationService")
@Slf4j
public class IMRChartCalculationService implements ControlChartCalculations {

	private static ControlChartResponse controlChart;
	private static int subGroup = 2;
	private static int roundOff = 2;
	private static double[] attributeValue;

	@Override
	public IMRChartCalculationService self() {
		return new IMRChartCalculationService();
	}

	private static IMRChartCalculationService IMRChartCalculationService(double[] array) {
		try {
			log.info("--> IMRChartCalculationService.IMRChartCalculationService()");
			// controlChart = ControlChartResponse.builder().attribute(array).build();
			attributeValue = array;
			return new IMRChartCalculationService();
		} catch (Exception e) {
			log.error("Error occurs in IMRChartCalculationService class -> IMRChartCalculationService method", e);
			throw new ChartCalulationException(e.getMessage());
		}
	}

	@Override
	public IMRChartCalculationService loadAttirbute(double[] array) {
		log.info("--> IMRChartCalculationService.loadAttirbute()");
		return IMRChartCalculationService(array);
	}

	@Override
	public ControlChartResponse getControlLimit() {
		try {
			log.info("--> IMRChartCalculationService.getControlLimit()");
			int subGroupSize = attributeValue.length - 1;
			double[] MR = ArrayUtils.toPrimitive(IntStream.range(0, subGroupSize)
					.mapToObj(idx -> idx < subGroupSize ? Math.abs(attributeValue[idx] - attributeValue[idx + 1]) : 0)
					.toArray(Double[]::new));

			double MR_Bar = Arrays.stream(MR).average().orElse(Double.NaN);
			double I_Bar = Arrays.stream(attributeValue).average().orElse(Double.NaN);

			double ucl_MR = ControlChartConstants.D4[subGroup] * MR_Bar;
			double lcl_MR = ControlChartConstants.D3[subGroup] * MR_Bar;
			double ucl_I = I_Bar + ControlChartConstants.A2[subGroup] * MR_Bar;
			double lcl_I = I_Bar - ControlChartConstants.A2[subGroup] * MR_Bar;

			return controlChart.builder()
					.attribute_I(ControlChartUtility.checkAttributeListWithControlLimit(attributeValue, ucl_I, lcl_I))
					.attribute_MR(
							ControlChartUtility.checkAttributeListWithControlLimit(attributeValue, ucl_MR, lcl_MR))
					.ucl_MR(ControlChartUtility.splitControlLimitRegion(3, ucl_MR, MR_Bar, roundOff))
					.mean_MR(ControlChartUtility.round(MR_Bar, roundOff))
					.lcl_MR(ControlChartUtility.splitControlLimitRegion(3, lcl_MR, MR_Bar, roundOff))
					.ucl_I(ControlChartUtility.splitControlLimitRegion(3, ucl_I, I_Bar, roundOff))
					.mean_I(ControlChartUtility.round(I_Bar, roundOff))
					.lcl_I(ControlChartUtility.splitControlLimitRegion(3, lcl_I, I_Bar, roundOff)).build();
		} catch (Exception e) {
			log.error("Error occurs in IMRChartCalculationService class -> getControlLimit method", e);
			throw new ChartCalulationException(e.getMessage());
		}
	}

	@Override
	public Object buildSubGroups(double[] array, int chunkSize) {
		// TODO Auto-generated method stub
		return null;
	}

}