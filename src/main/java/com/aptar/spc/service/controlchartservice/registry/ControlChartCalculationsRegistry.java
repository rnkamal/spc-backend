package com.aptar.spc.service.controlchartservice.registry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.aptar.spc.exception.DAOException;
import com.aptar.spc.service.interfaces.ControlChartCalculations;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ControlChartCalculationsRegistry {

	@Autowired
	@Qualifier("IMRChartCalculationService")
	private ControlChartCalculations<?> IMRChartCalculationService;

	@Autowired
	@Qualifier("XBarRChartCalculationService")
	private ControlChartCalculations<?> XBarRChartCalculationService;

	@Autowired
	@Qualifier("XBarSChartCalculationService")
	private ControlChartCalculations<?> XBarSChartCalculationService;

	public ControlChartCalculations<?> getControlChatService(String graphType) {
		log.info("-->ControlChartCalculationsRegistry.getControlChatService(" + graphType + ")");
		ControlChartCalculations<?> controlChartCalculations = null;
		switch (graphType.toLowerCase()) {
		case "i-mr":
			controlChartCalculations = IMRChartCalculationService;
			break;
		case "xbar-r":
			controlChartCalculations = XBarRChartCalculationService;
			break;
		case "xbar-s":
			controlChartCalculations = XBarSChartCalculationService;
			break;
		default:
			log.error("Please enter valid graph type.");
			throw new DAOException("Please enter valid graph type.");
		}
		return controlChartCalculations;
	}

	public Object calculateControlLimits(String graphType, double[] attributeValue) {
		return ((ControlChartCalculations<?>) getControlChatService(graphType).loadAttirbute(attributeValue))
				.getControlLimit();
	}

	public Object calculateControlLimits(String graphType, Integer subGroup, double[] attributeValue) {
		return ((ControlChartCalculations<?>) getControlChatService(graphType).buildSubGroups(attributeValue, 5))
				.getControlLimit();
	}

}
