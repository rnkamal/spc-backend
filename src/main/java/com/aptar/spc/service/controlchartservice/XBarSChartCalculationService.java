package com.aptar.spc.service.controlchartservice;

import java.util.HashMap;

import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.springframework.stereotype.Service;

import com.aptar.spc.DTO.response.ControlChartResponse;
import com.aptar.spc.constants.ControlChartConstants;
import com.aptar.spc.exception.ChartCalulationException;
import com.aptar.spc.service.interfaces.ControlChartCalculations;
import com.aptar.spc.util.ControlChartUtility;

import lombok.extern.slf4j.Slf4j;

@Service("XBarSChartCalculationService")
@Slf4j
public class XBarSChartCalculationService implements ControlChartCalculations {

	private static ControlChartResponse controlChart;
	private static int subGroup;
	private static int roundOff = 2;
	private static double[] attributeValue;
	private static HashMap<Integer, Object> groupedAttributeValue;

	@Override
	public XBarSChartCalculationService self() {
		log.info("--> XBarSChartCalculationService.self()");
		return new XBarSChartCalculationService();
	}

	private static XBarSChartCalculationService XBarSChartCalculationService(HashMap<Integer, Object> groupedArray) {
		try {
			log.info("--> XBarSChartCalculationService.XBarSChartCalculationService()");
			groupedAttributeValue = groupedArray;
			return new XBarSChartCalculationService();
		} catch (Exception e) {
			log.error("Error occurs in XBarSChartCalculationService class -> XBarSChartCalculationService method", e);
			throw new ChartCalulationException(e.getMessage());
		}
	}

	@Override
	public XBarSChartCalculationService buildSubGroups(double[] array, int chunkSize) {
		log.info("--> XBarSChartCalculationService.buildSubGroups()");
		subGroup = chunkSize;
		attributeValue = array;
		return XBarSChartCalculationService(ControlChartUtility.splitArray(array, chunkSize));
	}

	@Override
	public ControlChartResponse getControlLimit() {
		try {
			log.info("--> XBarSChartCalculationService.getControlLimit()");
			int subGroupSize = groupedAttributeValue.size();
			double[] SAry = new double[subGroupSize];
			double[] XBarAry = new double[subGroupSize];
			groupedAttributeValue.forEach((k, v) -> {
				StandardDeviation SStandDev = new StandardDeviation();
				Mean XBarMean = new Mean();
				SAry[k] = SStandDev.evaluate((double[]) v);
				XBarAry[k] = XBarMean.evaluate((double[]) v);
			});

			Mean XBarMean = new Mean();
			double XDoubleBar = XBarMean.evaluate(XBarAry);

			Mean SBarMean = new Mean();
			double SBar = SBarMean.evaluate(SAry);

			double ucl_XBar = XDoubleBar + ControlChartConstants.A3[subGroup] * SBar;
			double lcl_XBar = XDoubleBar - ControlChartConstants.A3[subGroup] * SBar;
			double ucl_S = ControlChartConstants.B4[subGroup] * SBar;
			double lcl_S = ControlChartConstants.B3[subGroup] * SBar;

			return controlChart.builder()
					.attribute_S(ControlChartUtility.checkAttributeListWithControlLimit(attributeValue, ucl_S, lcl_S))
					.attribute_XBar(
							ControlChartUtility.checkAttributeListWithControlLimit(attributeValue, ucl_XBar, lcl_XBar))
					.subGroups(groupedAttributeValue)
					.ucl_XBar(ControlChartUtility.splitControlLimitRegion(3, ucl_XBar, XDoubleBar, roundOff))
					.mean_XBar(ControlChartUtility.round(XDoubleBar, roundOff))
					.lcl_XBar(ControlChartUtility.splitControlLimitRegion(3, lcl_XBar, XDoubleBar, roundOff))
					.ucl_S(ControlChartUtility.splitControlLimitRegion(3, ucl_S, SBar, roundOff))
					.mean_S(ControlChartUtility.round(SBar, roundOff))
					.lcl_S(ControlChartUtility.splitControlLimitRegion(3, lcl_S, SBar, roundOff)).build();
		} catch (Exception e) {
			log.error("Error occurs in XBarSChartCalculationService class -> getControlLimit method", e);
			throw new ChartCalulationException(e.getMessage());
		}

	}

	@Override
	public Object loadAttirbute(double[] array) {
		// TODO Auto-generated method stub
		return null;
	}

}
