package com.aptar.spc.service.controlchartservice;

import java.util.HashMap;

import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.springframework.stereotype.Service;

import com.aptar.spc.DTO.response.ControlChartResponse;
import com.aptar.spc.constants.ControlChartConstants;
import com.aptar.spc.exception.ChartCalulationException;
import com.aptar.spc.service.interfaces.ControlChartCalculations;
import com.aptar.spc.util.ControlChartUtility;
import com.google.common.primitives.Doubles;

import lombok.extern.slf4j.Slf4j;

@Service("XBarRChartCalculationService")
@Slf4j
public class XBarRChartCalculationService implements ControlChartCalculations {

	private static ControlChartResponse controlChart;
	private static int subGroup;
	private static int roundOff = 2;
	private static double[] attributeValue;
	private static HashMap<Integer, Object> groupedAttributeValue;

	public XBarRChartCalculationService self() {
		return new XBarRChartCalculationService();
	}

	private static XBarRChartCalculationService XBarRChartCalculationService(HashMap<Integer, Object> groupedArray) {
		try {
			log.info("--> XBarRChartCalculationService.XBarRChartCalculationService()");
			groupedAttributeValue = groupedArray;
			return new XBarRChartCalculationService();
		} catch (Exception e) {
			log.error("Error occurs in XBarRChartCalculationService class -> XBarRChartCalculationService method", e);
			throw new ChartCalulationException(e.getMessage());
		}
	}

	@Override
	public XBarRChartCalculationService buildSubGroups(double[] array, int chunkSize) {
		log.info("--> XBarRChartCalculationService.buildSubGroups()");
		subGroup = chunkSize;
		attributeValue = array;
		return XBarRChartCalculationService(ControlChartUtility.splitArray(array, chunkSize));
	}

	@Override
	public ControlChartResponse getControlLimit() {
		try {
			log.info("--> XBarRChartCalculationService.getControlLimit()");
			int subGroupSize = groupedAttributeValue.size();
			double[] XBarAry = new double[subGroupSize];
			double[] RAry = new double[subGroupSize];
			groupedAttributeValue.forEach((k, v) -> {
				Mean xBarMean = new Mean();
				XBarAry[k] = xBarMean.evaluate((double[]) v);
				double R = Doubles.max((double[]) v) - Doubles.min((double[]) v);
				RAry[k] = R;
			});

			Mean XDoubleBarMean = new Mean();
			double XDoubleBar = XDoubleBarMean.evaluate(XBarAry);

			Mean RBarMean = new Mean();
			double RBar = RBarMean.evaluate(RAry);

			double ucl_XBar = XDoubleBar + ControlChartConstants.A2[subGroup] * RBar;
			double lcl_XBar = XDoubleBar - ControlChartConstants.A2[subGroup] * RBar;
			double ucl_R = ControlChartConstants.D4[subGroup] * RBar;
			double lcl_R = ControlChartConstants.D3[subGroup] * RBar;

			return controlChart.builder()
					.attribute_XBar(
							ControlChartUtility.checkAttributeListWithControlLimit(attributeValue, ucl_XBar, lcl_XBar))
					.attribute_R(ControlChartUtility.checkAttributeListWithControlLimit(attributeValue, ucl_R, lcl_R))
					.subGroups(groupedAttributeValue)
					.ucl_XBar(ControlChartUtility.splitControlLimitRegion(3, ucl_XBar, XDoubleBar, roundOff))
					.mean_XBar(ControlChartUtility.round(XDoubleBar, roundOff))
					.lcl_XBar(ControlChartUtility.splitControlLimitRegion(3, lcl_XBar, XDoubleBar, roundOff))
					.ucl_R(ControlChartUtility.splitControlLimitRegion(3, ucl_R, RBar, roundOff))
					.mean_R(ControlChartUtility.round(RBar, roundOff))
					.lcl_R(ControlChartUtility.splitControlLimitRegion(3, lcl_R, RBar, roundOff)).build();
		} catch (Exception e) {
			log.error("Error occurs in XBarRChartCalculationService class -> getControlLimit method", e);
			throw new ChartCalulationException(e.getMessage());
		}

	}

	@Override
	public Object loadAttirbute(double[] array) {
		// TODO Auto-generated method stub
		return null;
	}

}
