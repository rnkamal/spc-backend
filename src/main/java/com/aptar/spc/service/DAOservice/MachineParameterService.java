package com.aptar.spc.service.DAOservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aptar.spc.DAO.MachineParameterDAO;
import com.aptar.spc.entity.MachineParameter;
import com.aptar.spc.exception.DAOException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class MachineParameterService {

	@Autowired
	MachineParameterDAO machineParameterDAO;

	@Cacheable(value = "MachineParameterName", key = "'MachineParameter'+#MachineParameterId")
	public MachineParameter getMachineParameterById(Integer MachineParameterId) {
		try {
			log.info("--> MachineParameterService.getMachineParameterById()");
			return machineParameterDAO.getOne(MachineParameterId);
		} catch (Exception e) {
			log.error("Error occurs in MachineParameterService class --> getMachineParameterById method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	@Cacheable(value = "MachineParameter", key = "'MachineParameter'+#Tag")
	public MachineParameter getMachineParameterByTag(String Tag) {
		try {
			log.info("--> MachineParameterService.getMachineParameterIdByTag()");
			return machineParameterDAO.findByTag(Tag);
		} catch (Exception e) {
			log.error("Error occurs in MachineParameterService class --> getMachineParameterByTag method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}
}
