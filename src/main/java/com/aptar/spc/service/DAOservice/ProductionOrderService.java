package com.aptar.spc.service.DAOservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aptar.spc.DAO.ProductionOrderDAO;
import com.aptar.spc.entity.ProductionOrder;
import com.aptar.spc.exception.DAOException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class ProductionOrderService {

	@Autowired
	ProductionOrderDAO productionOrderDAO;

	public ProductionOrder getPoByName(String productionOrder) {
		try {
			log.info("-->ProductionOrderService.getPoByName()");
			return productionOrderDAO.findByProductionOrder(productionOrder);
		} catch (Exception e) {
			log.error("Error occurs in ProductionOrderService class --> getPoByName method", e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}
}
