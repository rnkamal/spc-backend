package com.aptar.spc.service.DAOservice;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aptar.spc.DAO.CombinationDAO;
import com.aptar.spc.DAO.CombinationRevisionDAO;
import com.aptar.spc.DAO.CombinationRevisionParameterRefDAO;
import com.aptar.spc.DTO.request.CombinationRevisionRequest;
import com.aptar.spc.DTO.response.CombinationRevisionParamResponse;
import com.aptar.spc.DTO.response.CombinationRevisionResponse;
import com.aptar.spc.DTO.wrapper.SpcSuccessResponse;
import com.aptar.spc.entity.CombinationRevision;
import com.aptar.spc.exception.DAOException;
import com.aptar.spc.mapper.CombinationRevisionParamMapper;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class CombinationRevisionService {

	@Autowired
	CombinationRevisionDAO combinationRevisionDAO;

	@Autowired
	CombinationRevisionParameterRefDAO combinationRevisionParameterRefDAO;

	@Autowired
	CombinationDAO combinationDAO;

	@Autowired
	RevisionParamService revisionParamService;

	@Autowired
	CombinationRevisionParamMapper combinationRevisionParamMapper;

	public CombinationRevisionResponse createNewRevision(CombinationRevisionRequest CombinationRevisionRequest) {
		try {
			log.info("--> CombinationRevisionService.createNewRevision()");
			return combinationRevisionParamMapper.toCombinationRevisionResponse(combinationRevisionDAO
					.saveAndFlush(combinationRevisionParamMapper.toCombinationRevision(CombinationRevisionRequest)));
		} catch (Exception e) {
			log.error("Error occurs in CombinationRevisionService class --> createNewRevision method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public SpcSuccessResponse<String> activateCombinationRevision(Integer Id) {
		Optional<CombinationRevision> combinationRevision = combinationRevisionDAO.findById(Id);
		if (combinationRevision.get().getActive().equals(false)) {
			CombinationRevision activeCombinationRevision = null;
			try {
				activeCombinationRevision = combinationRevisionDAO.findByCombinationIdAndActive(
						combinationDAO.findByCombinationRevisions(combinationRevision).getId(), true);
			} catch (IncorrectResultSizeDataAccessException e) {
				log.warn("No Combination Revision is activated yet..", e.getLocalizedMessage());
			}
			if (activeCombinationRevision != null && !activeCombinationRevision.getId().equals(Id)
					&& activeCombinationRevision.getActive().equals(true)) {
				activeCombinationRevision.setActive(false);
				combinationRevisionDAO.saveAndFlush(activeCombinationRevision);
			}

			combinationRevision.get().setActive(true);
			combinationRevisionDAO.saveAndFlush(combinationRevision.get());
			return new SpcSuccessResponse<String>("Successfully updated");
		} else {
			throw new DAOException("Already in active state");
		}
	}

	public List<CombinationRevisionResponse> getAllCombinationRevision(Integer CombinationId) throws DAOException {
		try {
			log.info("--> CombinationRevisionService.getAllCombinationRevision()");
			return combinationRevisionParamMapper.toCombinationRevisionResponseList(
					combinationRevisionDAO.findByCombination(combinationDAO.getOne(CombinationId)));
		} catch (Exception e) {
			log.error("Error occurs in CombinationRevisionService class --> getAllCombinationRevision method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public CombinationRevision getActiveCombinationRevision(Integer CombinationId) {
		try {
			log.info("--> CombinationRevisionService.getActiveCombinationRevision()");
			return combinationRevisionDAO.findByCombinationIdAndActive(CombinationId, true);
		} catch (IncorrectResultSizeDataAccessException e) {
			log.error(e.getLocalizedMessage());
			throw new DAOException("Combination id (" + CombinationId + ") has more than one active revision");
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			throw new DAOException(e.getLocalizedMessage());
		}
	}

	public CombinationRevisionParamResponse getActiveCombinationRevisionNdParams(Integer CombinationId)
			throws DAOException {
		try {
			log.info("--> CombinationRevisionService.getActiveCombinationRevisionNdParams()");
			CombinationRevision combinationRevision = getActiveCombinationRevision(CombinationId);
			return combinationRevisionParamMapper.toCombinationRevisionParamResponse(combinationRevision,
					revisionParamService.getAllCombinationRevisionParams(combinationRevision.getId()));
		} catch (Exception e) {
			log.error(
					"Error occurs in CombinationRevisionService class --> getActiveCombinationRevisionNdParams method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}
}
