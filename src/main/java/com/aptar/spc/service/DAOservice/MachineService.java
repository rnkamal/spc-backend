package com.aptar.spc.service.DAOservice;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aptar.spc.DAO.DepartmentDAO;
import com.aptar.spc.DAO.FunctionalLocDAO;
import com.aptar.spc.DAO.MachineDAO;
import com.aptar.spc.DAO.SiteDAO;
import com.aptar.spc.DTO.response.MachineWithSpcResponse;
import com.aptar.spc.constants.Constants;
import com.aptar.spc.entity.Combination;
import com.aptar.spc.entity.CombinationRevision;
import com.aptar.spc.entity.FunctionalLoc;
import com.aptar.spc.entity.Machine;
import com.aptar.spc.entity.Site;
import com.aptar.spc.exception.DAOException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class MachineService {

	@Autowired
	MachineDAO machineDAO;

	@Autowired
	SiteDAO siteDAO;

	@Autowired
	FunctionalLocDAO functionalLocDAO;

	@Autowired
	DepartmentDAO departmentDAO;

	@Autowired
	CombinationService combinationService;

	@Autowired
	CombinationRevisionService combinationRevisionService;

	@Autowired
	PoRevisionParamService poRevisionParamService;

	public List<Machine> getMachineByCombinations(List<Combination> combinations) {
		try {
			log.info("--> MachineService.getMachineByCombinations()");
			return machineDAO.findByCombinationsIn(combinations);
		} catch (Exception e) {
			log.error("Error occurs in MachineService class --> getMachineByCombinations method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public List<Machine> getAllMachineBySiteAndFunctLoc(List<Integer> Sites, List<Integer> FucntLoc) {
		try {
			log.info("--> MachineService.getAllMachine()");
			List<Machine> machines = new ArrayList<>();
			List<Site> Site = siteDAO.findAllById(Sites);
			List<FunctionalLoc> FunctionalLoc = functionalLocDAO.findAllById(FucntLoc);

			machines.addAll(machineDAO.findByDepartmentIn(departmentDAO.findBySiteIn(Site)));

			return machines.stream().filter(m -> FunctionalLoc.contains(m.getFunctionalLoc()))
					.collect(Collectors.toList());
		} catch (Exception e) {
			log.error("Error occurs in MachineService class --> getAllMachine method", e.getLocalizedMessage());
			throw new DAOException(e);
		}

	}

	public List<MachineWithSpcResponse> getAllMachineWithSpcStatus(List<Integer> Sites, List<Integer> FucntLoc) {
		try {
			log.info("--> MachineService.getAllMachineWithSpcStatus()");
			List<MachineWithSpcResponse> MachineWithSpcResponses = new ArrayList<>();
			List<Combination> combinations = combinationService
					.getAllCombinationByMachine(getAllMachineBySiteAndFunctLoc(Sites, FucntLoc));
			combinations.forEach(c -> {
				Integer combinationId = c.getId();
				CombinationRevision combinationRevision = combinationRevisionService
						.getActiveCombinationRevision(combinationId);
				if (combinationRevision != null) {
					Integer combinationRevisionId = combinationRevision.getId();

					MachineWithSpcResponses.add(MachineWithSpcResponse.builder().id(combinationId)
							.combination_revision_id(combinationRevisionId).product(c.getProduct())
							.po(c.getProductionOrder().getProductionOrder()).machine_name(c.getMachine().getName())
							.spc_status(poRevisionParamService.getSpcStatusByRevisionId(combinationRevisionId))
							.build());
				}
			});
			return sortMachineStatus(MachineWithSpcResponses);
		} catch (Exception e) {
			log.error("Error occurs in MachineService class --> getAllMachineWithSpcStatus method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	private static List<MachineWithSpcResponse> sortMachineStatus(List<MachineWithSpcResponse> machineWithSpcResponse) {
		Comparator<MachineWithSpcResponse> sortByCode = (obj1, obj2) -> Integer.compare(
				Constants.MACHINE_STATUS_MAP.get(obj1.getSpc_status()),
				Constants.MACHINE_STATUS_MAP.get(obj2.getSpc_status()));

		machineWithSpcResponse.sort(sortByCode);

		return machineWithSpcResponse;
	}

}
