package com.aptar.spc.service.DAOservice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.aptar.spc.DAO.PoCombinationRevisionParameterRefDAO;
import com.aptar.spc.DAO.SpcAlertDAO;
import com.aptar.spc.DTO.request.AlertFromHistorianRequest;
import com.aptar.spc.DTO.response.AlertResponse;
import com.aptar.spc.entity.PoCombinationRevisionParameterRef;
import com.aptar.spc.entity.SpcAlert;
import com.aptar.spc.exception.AlertSseException;
import com.aptar.spc.exception.DAOException;
import com.aptar.spc.mapper.SpcAlertMapper;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class SpcAlertService {

	public Map<String, SseEmitter> emitters = new HashMap<>();

	@Autowired
	SpcAlertDAO spcAlertDAO;

	@Autowired
	MachineParameterService machineParameterService;

	@Autowired
	PoRevisionParamService poRevisionParamService;

	@Autowired
	PoCombinationRevisionParameterRefDAO poRevisionParamDAO;

	@Autowired
	ProductionOrderService productionOrderService;

	@Autowired
	SpcAlertMapper spcAlertMapper;

	public AlertResponse createAlert(AlertFromHistorianRequest alertFromHistorianRequest) {
		try {
			log.info("-->SpcAlertService.getAllSpcRules()");
			PoCombinationRevisionParameterRef poCombinationRevisionParameterRef = poRevisionParamService
					.getPoRevisionParamByParameterIdAndPoId(
							machineParameterService.getMachineParameterByTag(alertFromHistorianRequest.getTag())
									.getId(),
							productionOrderService.getPoByName(alertFromHistorianRequest.getProductionOrder()));

			poCombinationRevisionParameterRef.setAlertStatus(true);

			poRevisionParamDAO.saveAndFlush(poCombinationRevisionParameterRef);

			AlertResponse alertResponse = spcAlertMapper.toSpcAlertResponse(spcAlertDAO.saveAndFlush(
					spcAlertMapper.toSpcAlertEntity(alertFromHistorianRequest, poCombinationRevisionParameterRef)));

			dispatchAlertDetailsToClients("1", alertResponse);

			return alertResponse;
		} catch (Exception e) {
			log.error("Error occurs in SpcAlertService class --> createAlert method", e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public SseEmitter subscribeAlert(String userID) {
		try {
			log.info("--> AlertSseService.subscribeAlert()");
			SseEmitter emitter = new SseEmitter(Long.MAX_VALUE);
			emitter.send(SseEmitter.event().name("Alert"));
			emitters.put(userID, emitter);

			emitter.onCompletion(() -> emitters.remove(emitter));
			emitter.onTimeout(() -> emitters.remove(emitter));
			emitter.onError((e) -> emitters.remove(emitter));
			getAlertDetailsToDb(userID);
			return emitter;
		} catch (Exception e) {
			log.error("Error occurs in AlertSseService class -> subscribeAlert method", e);
			throw new AlertSseException(e.getMessage());
		}
	}

	public void getAlertDetailsToDb(String userId) {
		log.info("--> AlertSseService.getAlertDetailsToDb()");
		List<SpcAlert> spcAlerts = spcAlertDAO.findAll();
		List<AlertResponse> alertResponse = new ArrayList<>();
		for (SpcAlert spcAlert : spcAlerts) {
			alertResponse.add(spcAlertMapper.toSpcAlertResponse(spcAlert));
		}
		SseEmitter emitter = emitters.get(userId);
		if (emitter != null) {
			try {
				emitter.send(SseEmitter.event().name("NewAlerts").data(alertResponse));
			} catch (Exception e) {
				emitters.remove(emitter);
				log.error("Error occurs in AlertSseService class -> dispatchAlertDetailsToClients method", e);
				throw new AlertSseException(e.getMessage());
			}
		}
	}

	private void dispatchAlertDetailsToClients(String userId, AlertResponse alertResponse) {
		log.info("--> AlertSseService.dispatchAlertDetailsToClients()");
		SseEmitter emitter = emitters.get(userId);
		if (emitter != null) {
			try {
				emitter.send(SseEmitter.event().name("NewAlerts").data(alertResponse));
			} catch (Exception e) {
				emitters.remove(emitter);
				log.error("Error occurs in AlertSseService class -> dispatchAlertDetailsToClients method", e);
				throw new AlertSseException(e.getMessage());
			}
		}
	}

}
