package com.aptar.spc.service.DAOservice;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aptar.spc.DAO.ContolChartTypeDAO;
import com.aptar.spc.DAO.GraphDAO;
import com.aptar.spc.DTO.response.ControlChartTypeResponse;
import com.aptar.spc.DTO.response.GraphResponse;
import com.aptar.spc.exception.DAOException;
import com.aptar.spc.mapper.ControlChartTypeMapper;
import com.aptar.spc.mapper.GraphMapper;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class ContolChartTypeService {

	@Autowired
	ContolChartTypeDAO contolChartTypeDAO;

	@Autowired
	GraphDAO graphDAO;

	@Autowired
	ControlChartTypeMapper controlChartTypeMapper;

	@Autowired
	GraphMapper graphMapper;

	@Cacheable(value = "ContolChartTypeName", key = "'ContolChartTypeId'+#ContolChartTypeId")
	public String getContolChartTypeNameById(Integer ContolChartTypeId) {
		try {
			log.info("-->ControlChartService.getContolChartTypeNameById()");
			return contolChartTypeDAO.getOne(ContolChartTypeId).getType();
		} catch (Exception e) {
			log.error("Error occurs in ControlChartService class --> getContolChartTypeNameById method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public List<ControlChartTypeResponse> getAllContolChartType() {
		try {
			log.info("-->ControlChartService.getAllContolChartType()");
			return contolChartTypeDAO.findAll().stream()
					.map(cc -> controlChartTypeMapper.toControlChartTypeResponse(cc)).collect(Collectors.toList());
		} catch (Exception e) {
			log.error("Error occurs in ControlChartService class --> getAllContolChartType method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public List<GraphResponse> getAllGraphByControlChart(Integer ControlChartId) {
		try {
			log.info("-->ControlChartService.getAllGraphByControlChart()");
			return graphDAO.findByControlChartTypeId(ControlChartId).stream().map(g -> graphMapper.toGraphResponse(g))
					.collect(Collectors.toList());
		} catch (Exception e) {
			log.error("Error occurs in ControlChartService class --> getAllGraphByControlChart method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

}
