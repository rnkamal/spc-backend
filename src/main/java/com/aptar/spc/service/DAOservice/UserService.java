package com.aptar.spc.service.DAOservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aptar.spc.DAO.DepartmentDAO;
import com.aptar.spc.DAO.FunctionalLocDAO;
import com.aptar.spc.DAO.MachineDAO;
import com.aptar.spc.DAO.SiteDAO;
import com.aptar.spc.DAO.UsersDAO;
import com.aptar.spc.DTO.response.FunctionalLocResponse;
import com.aptar.spc.DTO.response.SiteResponse;
import com.aptar.spc.entity.Department;
import com.aptar.spc.entity.FunctionalLoc;
import com.aptar.spc.entity.Machine;
import com.aptar.spc.entity.Site;
import com.aptar.spc.entity.User;
import com.aptar.spc.entity.UserRole;
import com.aptar.spc.exception.DAOException;
import com.aptar.spc.mapper.FunctionalLocMapper;
import com.aptar.spc.mapper.SiteMapper;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
@CacheConfig(cacheNames = "user")
public class UserService {

	@Autowired
	UsersDAO userDao;

	@Autowired
	UserRoleService userRoleService;

	@Autowired
	DepartmentDAO departmentDAO;

	@Autowired
	SiteDAO siteDAO;

	@Autowired
	FunctionalLocDAO functionalLocDAO;

	@Autowired
	MachineDAO machineDAO;

	@Autowired
	FunctionalLocMapper functionalLocMapper;

	@Autowired
	SiteMapper siteMapper;

	public boolean existByUserID(Integer Id) {
		try {
			log.info("--> UserService.existByUserID()");
			if (userDao.existsById(Id)) {
				return true;
			} else {
				throw new DAOException("User not exist");
			}
		} catch (Exception e) {
			log.error("Error occurs in UserService class --> existByUserID method", e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	@Cacheable(key = "'username'+#Id")
	public String getUserName(Integer Id) {
		try {
			log.info("--> UserService.getUserName()");
			if (existByUserID(Id)) {
				return userDao.getOne(Id).getFirstname();
			}
		} catch (Exception e) {
			log.error("Error occurs in UserService class method getUserName : " + e.getMessage());
			throw new DAOException(e);
		}
		return null;
	}

	@Cacheable(key = "'user_'+#UserName")
	public User getUserByUserName(String UserName) {
		try {
			log.info("--> UserService.getUserByUserName()");
			return userDao.findByFirstname(UserName).get(0);
		} catch (Exception e) {
			log.error("Error occurs in UserService class method getUserName : " + e.getMessage());
			throw new DAOException(e);
		}
	}

	private List<Department> getAssignedDepartments(User user) {
		try {
			log.info("--> UserService.getAssignedDepartments()");
			List<Department> departments = null;
			List<UserRole> userRoles = userRoleService.getRoles(user);
			if (!userRoles.isEmpty()) {
				departments = departmentDAO.findByUserRolesIn(userRoles);
			}
			return departments;
		} catch (Exception e) {
			log.error("Error occurs in UserService class --> getAssignedDepartments method", e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	private List<Site> getAssignedSites(User user) {
		try {
			log.info("--> UserService.getAssignedSites()");
			List<Site> sites = null;
			List<UserRole> userRoles = userRoleService.getRoles(user);
			if (!userRoles.isEmpty()) {
				sites = siteDAO.findByUserRolesIn(userRoles);
			}
			return sites;
		} catch (Exception e) {
			log.error("Error occurs in UserService class --> getAssignedSites method", e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public List<Department> getDepartments(User user) {
		try {
			log.info("--> UserService.getDepartments()");
			List<Department> departments = getAssignedDepartments(user);
			List<Department> tempDepartments = departmentDAO.findBySiteIn(getAssignedSites(user));
			if (!tempDepartments.isEmpty()) {
				departments.addAll(tempDepartments);
			}
			return departments;
		} catch (Exception e) {
			log.error("Error occurs in UserService class --> getDepartments method", e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public List<SiteResponse> getSite() {
		try {
			log.info("--> UserService.getSite()");
			User user = userDao.getOne(1);
			List<Site> sites = getAssignedSites(user);
			List<Department> departments = getAssignedDepartments(user);
			if (!departments.isEmpty()) {
				departments.stream().filter(d -> sites.contains(d.getSite())).forEach(d -> sites.add(d.getSite()));
			}
			List<SiteResponse> siteResponse = new ArrayList<>();
			sites.forEach(s -> {
				siteResponse.add(siteMapper.toSiteResponse(s));
			});
			return siteResponse;
		} catch (Exception e) {
			log.error("Error occurs in UserService class --> getSite method", e.getLocalizedMessage());
			throw new DAOException(e);
		}

	}

	public List<Machine> getMachinesByDepartment(List<Department> departmentList) {
		try {
			log.info("--> UserService.getMachinesite()");
			return machineDAO.findByDepartmentIn(departmentList);
		} catch (Exception e) {
			log.error("Error occurs in UserService class --> getMachinesByDepartment method", e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public List<FunctionalLoc> getFucntionLocByMachine(List<Machine> machineList) {
		try {
			log.info("--> UserService.getFucntionLocByMachineId()");
			return functionalLocDAO.findByMachinesIn(machineList);
		} catch (Exception e) {
			log.error("Error occurs in UserService class --> getFucntionLocByMachine method", e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public List<FunctionalLocResponse> getFunctionLocBySiteId(Integer siteId) {
		try {
			log.info("--> UserService.getFucntionLocBySiteId()");
			List<FunctionalLocResponse> FunctionalLocResponses = new ArrayList<>();
			List<FunctionalLoc> functionalLoc = functionalLocDAO
					.findByMachinesIn(getMachinesByDepartment(siteDAO.findById(siteId).get().getDepartments()));
			functionalLoc.forEach(f -> {
				FunctionalLocResponses.add(functionalLocMapper.toFunctionLocResponse(f));
			});
			return FunctionalLocResponses;
		} catch (Exception e) {
			log.error("Error occurs in UserService class --> getFunctionLocBySiteId method", e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

}
