package com.aptar.spc.service.DAOservice;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.aptar.spc.DAO.CombinationRevisionDAO;
import com.aptar.spc.DAO.CombinationRevisionParameterRefDAO;
import com.aptar.spc.DAO.ContolChartTypeDAO;
import com.aptar.spc.DTO.request.RevisionParamRequest;
import com.aptar.spc.DTO.response.Attribute;
import com.aptar.spc.DTO.response.ControlChartResponse;
import com.aptar.spc.DTO.response.RevisionParamResponse;
import com.aptar.spc.DTO.wrapper.SpcSuccessResponse;
import com.aptar.spc.entity.CombinationRevisionParameterRef;
import com.aptar.spc.exception.DAOException;
import com.aptar.spc.mapper.CombinationRevisionParamMapper;
import com.aptar.spc.service.controlchartservice.registry.ControlChartCalculationsRegistry;
import com.aptar.spc.util.ExcelUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class RevisionParamService {

	@Autowired
	UploadExcelService uploadExcelService;

	@Autowired
	ControlChartCalculationsRegistry controlChartCalculationsRegistry;

	@Autowired
	CombinationRevisionParameterRefDAO combinationRevisionParameterRefDAO;

	@Autowired
	CombinationRevisionParamMapper combinationRevisionParamMapper;

	@Autowired
	CombinationRevisionDAO combinationRevisionDAO;

	@Autowired
	ContolChartTypeDAO contolChartTypeDAO;

	public List<RevisionParamResponse> getAllCombinationRevisionParams(Integer combinationRevisionId)
			throws DAOException {
		try {
			log.info("--> RevisionParamService.getAllCombinationRevisionParams()");
			List<Integer> combinationRevisionIds = combinationRevisionDAO
					.getCombinationRevisionId(combinationRevisionId);
			return combinationRevisionParamMapper.toRevisionParamResponseList(
					combinationRevisionParameterRefDAO.findAllByCombinationRevisionIdIn(combinationRevisionIds));
		} catch (Exception e) {
			log.error("Error occurs in RevisionParamService class --> getAllCombinationRevisionParams method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public RevisionParamResponse getRevisionParamById(Integer Id) {
		try {
			CombinationRevisionParameterRef combinationRevisionParameterRef = combinationRevisionParameterRefDAO
					.findById(Id).get();
			return combinationRevisionParamMapper
					.toRevisionParamResponse(combinationRevisionParameterRefDAO.findById(Id).get());
		} catch (Exception e) {
			log.error("Error occurs in RevisionParamService class --> getRevisionParamById method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public SpcSuccessResponse updateRevisionParamById(RevisionParamRequest revisionParamRequest) {
		if (combinationRevisionParameterRefDAO.existsById(revisionParamRequest.getId())) {
			try {
				combinationRevisionParameterRefDAO.saveAndFlush(
						combinationRevisionParamMapper.toCombinationRevisionParameterRef(revisionParamRequest));
				return new SpcSuccessResponse(String.class, "Successfully updated");
			} catch (Exception e) {
				log.error("Error in Update RevisionParam :" + e.getMessage());
				throw new DAOException("Error in Update RevisionParam ");
			}
		} else {
			throw new DAOException("Revision Param ID is not exists");
		}
	}

	public ControlChartResponse reCalculateControlLimit(List<Attribute> controlChartRequest, Integer subGroup,
			Integer excelId, String controlChartType) {
		try {
			log.info("-->RevisionParam.reCalculateControlLimit()");
			List<Attribute> filteredAttribute = controlChartRequest.stream()
					.filter(request -> !(request.isControlLimitStatus())).collect(Collectors.toList());
			double[] dataPointsAry = filteredAttribute.stream().mapToDouble(Attribute::getAttibuteValue).toArray();
			ControlChartResponse controlChartResponse;
			if (subGroup == null || subGroup == 0) {
				controlChartResponse = (ControlChartResponse) controlChartCalculationsRegistry
						.calculateControlLimits(controlChartType, dataPointsAry);
			} else {
				controlChartResponse = (ControlChartResponse) controlChartCalculationsRegistry
						.calculateControlLimits(controlChartType, subGroup, dataPointsAry);
			}
			uploadExcelService.updateRecalculated(excelId);
			return controlChartResponse;
		} catch (Exception e) {
			log.error("Error occurs in RevisionParamService class --> reCalculateControlLimit method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public ControlChartResponse calculateControlLimit(MultipartFile file, Integer subGroup, String controlChartType) {
		try {
			log.info("-->RevisionParam.calculateControlLimit()");
			List<Double> dataPoints = ExcelUtil.hasExcelFormat(file).parseExcel();
//			Optional<CombinationRevisionParameterRef> revisionParam = combinationRevisionParameterRefDAO
//					.findById(revisionParamId);
//			contolChartTypeDAO.findByCombinationRevisionParameterRefs(revisionParam.get());
			double[] dataPointsAry = dataPoints.stream().mapToDouble(d -> d).toArray();
			ControlChartResponse controlChartResponse;
			if (subGroup == null || subGroup == 0) {
				controlChartResponse = (ControlChartResponse) controlChartCalculationsRegistry
						.calculateControlLimits(controlChartType, dataPointsAry);
			} else {
				controlChartResponse = (ControlChartResponse) controlChartCalculationsRegistry
						.calculateControlLimits(controlChartType, subGroup, dataPointsAry);
			}
			controlChartResponse.setExcelId(uploadExcelService.save(file));
			return controlChartResponse;
		} catch (Exception e) {
			log.error("Error occurs in RevisionParamService class --> calculateControlLimit method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public RevisionParamResponse editRevisionParamById(Integer Id) {
		try {
			return combinationRevisionParamMapper
					.toRevisionParamResponse(combinationRevisionParameterRefDAO.getOne(Id));
		} catch (Exception e) {
			log.error("Error occurs in RevisionParamService class method editRevisionParamById",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

}
