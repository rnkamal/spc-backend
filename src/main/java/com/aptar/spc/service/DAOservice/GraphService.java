package com.aptar.spc.service.DAOservice;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aptar.spc.DAO.GraphDAO;
import com.aptar.spc.DTO.response.GraphResponse;
import com.aptar.spc.exception.DAOException;
import com.aptar.spc.mapper.GraphMapper;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class GraphService {

	@Autowired
	GraphDAO graphDAO;

	@Autowired
	GraphMapper graphMapper;

	@Cacheable(value = "GraphListService")
	public List<GraphResponse> getAllGraphs() {
		try {
			log.info("-->GraphService.getAllSpcRules()");
			return graphDAO.findAll().stream().map(sp -> graphMapper.toGraphResponse(sp)).collect(Collectors.toList());
		} catch (Exception e) {
			log.error("Error occurs in GraphService class --> getAllSpcRules method", e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	@Cacheable(value = "GraphName", key = "'GraphId'+#id")
	public String getGraphName(Integer id) {
		try {
			log.info("--> GraphService.getGraphName()");
			return graphDAO.getOne(id).getType();
		} catch (Exception e) {
			log.error("Error occurs in GraphService class --> getGraphName method", e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

}
