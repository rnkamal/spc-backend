package com.aptar.spc.service.DAOservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aptar.spc.DAO.DepartmentDAO;
import com.aptar.spc.DAO.UserRoleDAO;
import com.aptar.spc.entity.User;
import com.aptar.spc.entity.UserRole;
import com.aptar.spc.exception.DAOException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class UserRoleService {

	@Autowired
	UserService userService;

	@Autowired
	UserRoleDAO userRoleDAO;

	@Autowired
	DepartmentDAO departmentDAO;

	public List<UserRole> getRoles(User user) {
		try {
			log.info("--> UserRoleService.getRoles()");
			List<UserRole> userRoles = null;
			if (userService.existByUserID(user.getId())) {
				userRoles = userRoleDAO.findByUser(user);
			}
			return userRoles;
		} catch (Exception e) {
			log.error("Error occurs in UserRoleService class --> getRoles method", e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

}
