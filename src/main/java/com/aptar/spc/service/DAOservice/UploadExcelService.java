package com.aptar.spc.service.DAOservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.aptar.spc.DAO.UploadExcelDAO;
import com.aptar.spc.entity.UploadExcel;
import com.aptar.spc.exception.DAOException;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class UploadExcelService {

	@Autowired
	UploadExcelDAO uploadExcelDAO;

	public Integer save(MultipartFile file) {
		try {
			UploadExcel uploadExcel = new UploadExcel();
			uploadExcel.setCombinationRevisionParameterRef(null);
			uploadExcel.setFile(file.getBytes());
			return uploadExcelDAO.saveAndFlush(uploadExcel).getId();
		} catch (Exception e) {
			log.error("Error in save excel in DB : " + e.getLocalizedMessage());
			throw new DAOException("Error in save excel in DB");
		}
	}

	public Integer updateRecalculated(Integer excelId) {
		try {
			UploadExcel uploadExcel = uploadExcelDAO.findById(excelId).get();
			uploadExcel.setRecalculated(true);
			return uploadExcelDAO.saveAndFlush(uploadExcel).getId();
		} catch (Exception e) {
			log.error("Error in save excel in DB : " + e.getLocalizedMessage());
			throw new DAOException("Error in save excel in DB");
		}
	}
}
