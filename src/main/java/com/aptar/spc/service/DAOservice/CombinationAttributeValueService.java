package com.aptar.spc.service.DAOservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aptar.spc.DAO.CombinationAttributeDAO;
import com.aptar.spc.DAO.CombinationAttributeValueDAO;
import com.aptar.spc.entity.Combination;
import com.aptar.spc.entity.CombinationAttributeValue;
import com.aptar.spc.exception.DAOException;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class CombinationAttributeValueService {

	@Autowired
	CombinationAttributeValueDAO combinationAttributeValueDAO;

	@Autowired
	CombinationAttributeDAO combinationAttributeDAO;

	public List<CombinationAttributeValue> getAllCombinationAttributeValues(List<Combination> combination) {
		try {
			log.info("--> CombinationAttributeValueService.getAllCombinationAttributeValues()");
			List<CombinationAttributeValue> combinationAttributeValue = combinationAttributeValueDAO
					.findByCombinationIn(combination);
			return combinationAttributeValue;
		} catch (Exception e) {
			log.error("Error occurs in CombinationAttributeValueService", e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

}
