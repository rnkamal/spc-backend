package com.aptar.spc.service.DAOservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aptar.spc.DAO.CombinationDAO;
import com.aptar.spc.DAO.ProductionOrderDAO;
import com.aptar.spc.DTO.response.CombinationResponse;
import com.aptar.spc.entity.Combination;
import com.aptar.spc.entity.Machine;
import com.aptar.spc.entity.ProductionOrder;
import com.aptar.spc.exception.DAOException;
import com.aptar.spc.mapper.CombinationMapper;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class CombinationService {

	@Autowired
	CombinationMapper combinationMapper;

	@Autowired
	CombinationAttributeValueService combinationAttributeValueService;

	@Autowired
	CombinationDAO combinationDAO;

	@Autowired
	ProductionOrderDAO productionOrderDAO;

	@Autowired
	MachineService machineService;

	@SuppressWarnings("null")
	public CombinationResponse getAllCombination(List<Integer> Sites, List<Integer> FucntLoc) {
		try {
			log.info("--> CombinationService.getAllCombination()");
			List<Combination> combination = new ArrayList<>();
			List<Machine> machinesWithSelectedFucntionalLoc = machineService.getAllMachineBySiteAndFunctLoc(Sites,
					FucntLoc);
			combination = getAllCombinationByMachine(machinesWithSelectedFucntionalLoc);
			return combinationMapper.toCombinationResponse(combination);
		} catch (Exception e) {
			log.error("Error occurs in CombinationService class --> getAllCombination method", e.getLocalizedMessage());
			throw new DAOException(e);
		}

	}

	public List<Combination> getAllCombinationByMachine(List<Machine> machines) {
		try {
			log.info("--> CombinationService.getAllCombinationByMachine()");
			return combinationDAO.findByMachineIn(machines);
		} catch (Exception e) {
			log.error("Error occurs in CombinationService class --> getAllCombinationByMachine method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public CombinationResponse getCombinationById(Integer id) {
		try {
			log.info("--> CombinationService.getCombinationById()");
			return combinationMapper.toCombinationResponse(combinationDAO.findById(id).get());
		} catch (Exception e) {
			log.error("Error occurs in CombinationService class --> getCombinationById method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public CombinationResponse getCombinationWithSpcStatusById(Integer id) {
		try {
			log.info("--> CombinationService.getCombinationWithSpcStatusById()");
			return combinationMapper.toCombinationWithSpcStatusResponse(combinationDAO.findById(id).get());
		} catch (Exception e) {
			log.error("Error occurs in CombinationService class --> getCombinationWithSpcStatusById method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public List<ProductionOrder> getPoByCombinations(List<Combination> combinations) {
		try {
			log.info("--> CombinationService.getPoByCombinationId()");
			return productionOrderDAO.findByCombinationsIn(combinations);
		} catch (Exception e) {
			log.error("Error occurs in CombinationService class --> getPoByCombinationId method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

}
