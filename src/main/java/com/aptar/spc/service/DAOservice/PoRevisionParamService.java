package com.aptar.spc.service.DAOservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aptar.spc.DAO.CombinationRevisionDAO;
import com.aptar.spc.DAO.PoCombinationRevisionParameterRefDAO;
import com.aptar.spc.DTO.response.PoRevisionParamDropdownResponse;
import com.aptar.spc.DTO.response.PoRevisionParamResponse;
import com.aptar.spc.DTO.wrapper.SpcSuccessResponse;
import com.aptar.spc.constants.Constants;
import com.aptar.spc.entity.PoCombinationRevisionParameterRef;
import com.aptar.spc.entity.ProductionOrder;
import com.aptar.spc.exception.DAOException;
import com.aptar.spc.mapper.PoCombinationRevisionParamMapper;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class PoRevisionParamService {

	@Autowired
	PoCombinationRevisionParameterRefDAO poCombinationRevisionParameterRefDAO;

	@Autowired
	CombinationRevisionDAO combinationRevisionDAO;

	@Autowired
	PoCombinationRevisionParamMapper poCombinationRevisionParamMapper;

	public String getSpcStatusByRevisionId(Integer Id) {
		try {
			log.info("--> PoRevisionParamService.getSpcStatusByRevisionId()");
			String result;
			if (poCombinationRevisionParameterRefDAO.countByCombinationRevisionIdAndSpcAndActive(Id, true, true) >= 1) {
				if (poCombinationRevisionParameterRefDAO.countByCombinationRevisionIdAndSpcAndActiveAndAlertStatus(Id,
						true, true, true) >= 1) {
					result = Constants.SPC_STATUS_ACTIVE_WITH_ALERT;
				} else {
					result = Constants.SPC_STATUS_ACTIVE;
				}
			} else {
				result = Constants.SPC_STATUS_INACTIVE;
			}
			return result;
		} catch (Exception e) {
			log.error(
					"Error occurs in PoRevisionParamService class getSpcStatusByRevisionId method editRevisionParamById",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public SpcSuccessResponse<String> deactivateSpcAlertByRevisionId(Integer combinationRevisionId) {
		try {
			log.info("--> PoRevisionParamService.deactivateSpcAlertByRevisionId()");
			poCombinationRevisionParameterRefDAO.updateSpc(false, combinationRevisionId);
			return new SpcSuccessResponse<String>("Successfully Deactivated");
		} catch (Exception e) {
			log.error(
					"Error occurs in PoRevisionParamService class deactivateSpcAlertByRevisionId method editRevisionParamById",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}

	}

	public List<PoRevisionParamResponse> getAllSpcEnablePoCombinationRevisionParams(Integer combinationRevisionId)
			throws DAOException {
		try {
			log.info("--> PoRevisionParamService.getAllSpcEnablePoCombinationRevisionParams()");
			List<Integer> combinationRevisionIds = combinationRevisionDAO
					.getCombinationRevisionId(combinationRevisionId);
			return poCombinationRevisionParamMapper.toPoRevisionParamResponseList(poCombinationRevisionParameterRefDAO
					.findAllByCombinationRevisionIdInAndSpc(combinationRevisionIds, true));
		} catch (Exception e) {
			log.error(
					"Error occurs in PoRevisionParamService class --> getAllSpcEnablePoCombinationRevisionParams method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public List<PoRevisionParamResponse> getPoRevisionParamById(List<Integer> PoRevisionParamId) {
		try {
			log.info("--> PoRevisionParamService.getPoRevisionParamById()");
			return poCombinationRevisionParamMapper.toPoRevisionParamResponseList(
					poCombinationRevisionParameterRefDAO.findAllByIdIn(PoRevisionParamId));
		} catch (Exception e) {
			log.error("Error occurs in PoRevisionParamService class --> getPoRevisionParamById method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public List<PoRevisionParamDropdownResponse> getAllSpcEnablePoCombinationRevisionParamsDropdown(
			Integer combinationRevisionId) throws DAOException {
		try {
			log.info("--> PoRevisionParamService.getAllSpcEnablePoCombinationRevisionParamsDropdown()");
			List<Integer> combinationRevisionIds = combinationRevisionDAO
					.getCombinationRevisionId(combinationRevisionId);
			return poCombinationRevisionParamMapper
					.toPoRevisionParamDropdownResponseResponseList(poCombinationRevisionParameterRefDAO
							.findIdAndPrameterIdByCombinationRevisionIdInAndSpc(combinationRevisionIds, true));
		} catch (Exception e) {
			log.error(
					"Error occurs in PoRevisionParamService class --> getAllSpcEnablePoCombinationRevisionParamsDropdown method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public PoCombinationRevisionParameterRef getPoRevisionParamByParameterIdAndPoId(Integer ParameterId,
			ProductionOrder PoId) {
		try {
			log.info("--> PoRevisionParamService.getPoRevisionParamById()");
			return poCombinationRevisionParameterRefDAO.findByParameterIdAndProductionOrder(ParameterId, PoId);
		} catch (Exception e) {
			log.error("Error occurs in PoRevisionParamService class --> getPoRevisionParamById method",
					e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}
}
