package com.aptar.spc.service.DAOservice;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aptar.spc.DAO.SpcRuleDAO;
import com.aptar.spc.DTO.response.SpcRuleResponse;
import com.aptar.spc.entity.SpcRule;
import com.aptar.spc.exception.DAOException;
import com.aptar.spc.mapper.SpcRuleMapper;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class SpcRuleService {

	@Autowired
	SpcRuleDAO spcRuleDAO;

	@Autowired
	SpcRuleMapper spcRuleMapper;

	public List<SpcRuleResponse> getAllSpcRules() {
		try {
			log.info("-->SpcRuleService.getAllSpcRules()");
			return spcRuleDAO.findAll().stream().map(sp -> spcRuleMapper.toSpcRuleResponse(sp))
					.collect(Collectors.toList());
		} catch (Exception e) {
			log.error("Error occurs in SpcRuleService class --> getSpcRuleByID method", e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public SpcRule getSpcRuleByID(Integer id) {
		try {
			log.info("-->SpcRuleService.getSpcRuleByID(" + id + ")");
			return spcRuleDAO.getOne(id);
		} catch (Exception e) {
			log.error("Error occurs in SpcRuleService class --> getSpcRuleByID method", e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

	public List<String> getSpcRuleByID(Integer[] ids) {
		try {
			log.info("-->SpcRuleService.getSpcRuleByID(" + ids + ")");
			return spcRuleDAO.getSpcRuleName(ids);
		} catch (Exception e) {
			log.error("Error occurs in SpcRuleService class --> getSpcRuleByID method", e.getLocalizedMessage());
			throw new DAOException(e);
		}
	}

}
