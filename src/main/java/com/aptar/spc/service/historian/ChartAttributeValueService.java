package com.aptar.spc.service.historian;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.aptar.spc.DTO.historian.response.ItemList;
import com.aptar.spc.DTO.historian.response.ItemListChartResponse;
import com.aptar.spc.constants.Constants;
import com.aptar.spc.mapper.HistorianMapper;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
//@CacheConfig(cacheNames = "ChartAttributeValueService")
public class ChartAttributeValueService {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	HistorianMapper historianMapper;

	@Autowired
	ChartAttributeService chartAttributeService;

	public ItemListChartResponse getChartValuesByTime(String machieName, String machineTag, String chart, String time,
			Integer maxCount) {
		log.info("--> HistorianService.getChartValuesByTime()");
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromUriString(Constants.URL + "/streams/"
						+ chartAttributeService.addChartWebIdToCache(machieName, machineTag, chart) + "/recorded")
				.queryParam("time", time).queryParam("maxCount", maxCount)
				.queryParam("selectedFields", "Items.Value;Items.Timestamp;");
		ItemListChartResponse itemListResponse = historianMapper.toItemListChartResponseResponse(
				restTemplate.getForEntity(builder.build().toUri(), ItemList.class).getBody());
		itemListResponse.setChartName(chart);
		return itemListResponse;
	}

//	@Cacheable(key = "'chartValue_'+#chart+'_'+#startTime+'_'+#maxCount")
	public ItemListChartResponse getChartValuesAfterTimeStamp(String machieName, String machineTag, String chart,
			String startTime, Integer maxCount) {
		log.info("--> HistorianService.getChartValuesAfterStartTime()");
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromUriString(Constants.URL + "/streams/"
						+ chartAttributeService.addChartWebIdToCache(machieName, machineTag, chart) + "/recorded")
				.query("startTime=" + startTime).queryParam("maxCount", maxCount + 1)
				.queryParam("selectedFields", "Items.Value;Items.Timestamp;");
		ItemListChartResponse itemListResponse = historianMapper.toItemListChartResponseResponse(
				restTemplate.getForEntity(builder.build().toUriString(), ItemList.class).getBody());
		itemListResponse.setChartName(chart);
		itemListResponse.getItem().removeIf(p -> (p.getTimestamp().compareTo(startTime) <= 0));
		return itemListResponse;
	}

}
