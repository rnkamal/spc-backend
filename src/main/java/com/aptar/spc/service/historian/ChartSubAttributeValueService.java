package com.aptar.spc.service.historian;

import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.aptar.spc.DTO.historian.request.SubAttribute;
import com.aptar.spc.constants.Constants;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ChartSubAttributeValueService {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	ChartSubAttributeService chartSubAttributeService;

	public void setSubAttributeValue(String machieName, String machineTag, String chart, String paramter,
			Double value) {
		log.info("--> HistorianService.setSubAttributeValue()");
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(Constants.URL + "/attributes/"
				+ chartSubAttributeService.getSubAttributeWebID(machieName, machineTag, chart, paramter) + "/value");
		restTemplate.put(builder.build().toUri(), SubAttribute.builder().timestamp(Instant.now().toString())
				.unitsAbbreviation("mm").value(value).build());
	}

}
