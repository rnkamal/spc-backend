package com.aptar.spc.service.historian;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.aptar.spc.DTO.historian.response.ItemList;
import com.aptar.spc.constants.Chart;
import com.aptar.spc.constants.Constants;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@CacheConfig(cacheNames = "ChartAttributeService")
public class ChartAttributeService {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	InjectionUnitService injectionUnitService;

	@Cacheable(key = "'chart_'+#chart")
	public String addChartWebIdToCache(String machieName, String machineTag, String chart) {
		log.info("--> HistorianService.addChartWebIdToCache()");
		String webId = null;
		ResponseEntity<ItemList> response = getChartWebId(
				injectionUnitService.addInjectionUnitWebIdToCache(machieName, machineTag), chart);
		switch (response.getStatusCode()) {
		case OK:
			try {
				webId = response.getBody().getItem().get(0).getWebId();
				break;
			} catch (Exception e) {
				e.printStackTrace();
			}
		case INTERNAL_SERVER_ERROR:
			try {
				webId = getChartWebId(injectionUnitService.putInjectionUnitWebIdToCache(machieName, machineTag), chart)
						.getBody().getItem().get(0).getWebId();
				break;
			} catch (Exception e) {
				e.printStackTrace();
			}
		default:
			break;
		}
		return webId;
	}

	@CachePut(key = "'chart_'+#chart")
	public String updateChartWebIdToCache(String machieName, String machineTag, String chart) {
		log.info("--> HistorianService.addChartWebIdToCache()");
		String webId = null;
		return getChartWebId(injectionUnitService.putInjectionUnitWebIdToCache(machieName, machineTag), chart).getBody()
				.getItem().get(0).getWebId();

	}

	public ResponseEntity<ItemList> getChartWebId(String InjectionUnitWebId, String chart) {
		log.info("--> HistorianService.getChartWebId()");
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromUriString(Constants.URL + "/attributes/" + InjectionUnitWebId + "/attributes")
				.queryParam("nameFilter", Chart.valueOf(chart).get())
				.queryParam("selectedFields", "Items.Name;Items.WebId;");
		return restTemplate.getForEntity(builder.build().toUri(), ItemList.class);
	}
}
