package com.aptar.spc.service.historian;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.aptar.spc.DTO.historian.response.ItemList;
import com.aptar.spc.constants.Constants;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@CacheConfig(cacheNames = "ChartSubAttributeService")
public class ChartSubAttributeService {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	ChartAttributeService chartAttributeService;

	@Cacheable(key = "'paramterWebID_'+#machieName+'_'+#machineTag+'_'+#chart+'_'+#paramter")
	public String getSubAttributeWebID(String machieName, String machineTag, String chart, String paramter) {
		log.info("--> HistorianService.getSubAttributeWebID()");
		String webId = null;
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromUriString(Constants.URL + "/attributes/"
						+ chartAttributeService.addChartWebIdToCache(machieName, machineTag, chart) + "/attributes")
				.queryParam("nameFilter", paramter).queryParam("selectedFields", "Items.Name;Items.WebId;");
		ResponseEntity<ItemList> response = restTemplate.getForEntity(builder.build().toUri(), ItemList.class);
		switch (response.getStatusCode()) {
		case OK:
			try {
				webId = response.getBody().getItem().get(0).getWebId();
				break;
			} catch (Exception e) {
				e.printStackTrace();
			}
		case INTERNAL_SERVER_ERROR:
			try {
				UriComponentsBuilder rebuilder = UriComponentsBuilder.fromUriString(Constants.URL + "/attributes/"
						+ chartAttributeService.updateChartWebIdToCache(machieName, machineTag, chart) + "/attributes")
						.queryParam("nameFilter", paramter).queryParam("selectedFields", "Items.Name;Items.WebId;");
				;
				webId = restTemplate.getForEntity(rebuilder.build().toUri(), ItemList.class).getBody().getItem().get(0)
						.getWebId();
				break;
			} catch (Exception e) {
				e.printStackTrace();
			}
		default:
			break;
		}
		return webId;
	}

}
