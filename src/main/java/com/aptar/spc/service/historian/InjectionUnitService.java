package com.aptar.spc.service.historian;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.aptar.spc.DTO.historian.response.BatchResponse;
import com.aptar.spc.constants.Constants;
import com.aptar.spc.util.CommonUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@CacheConfig(cacheNames = "InjectionUnitService")
public class InjectionUnitService {

	@Value("classpath:batchJson/InjectionUnitWebId.Json")
	Resource resourceFile;

	@Autowired
	RestTemplate restTemplate;

	@Cacheable(key = "'InjectionUnitWebId'")
	public String addInjectionUnitWebIdToCache(String MachieName, String MachineTag) {
		log.info("--> HistorianService.addInjectionUnitWebIdToCache()");
		HashMap<String, String> replaceAttributes = new HashMap<String, String>();
		replaceAttributes.put(Constants.MACHINE_NAME, MachieName);
		replaceAttributes.put(Constants.MACHINE_TAG, MachineTag);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(CommonUtil.replaceResource(resourceFile, replaceAttributes),
				headers);
		return restTemplate.postForEntity(Constants.URL + "/batch", entity, BatchResponse.class).getBody()
				.getInjectionUnitAttributes().getContentWithHeader().getItemsWithHeader().get(0).getContent().getItems()
				.get(0).getWebId();
	}

	@CachePut(key = "'InjectionUnitWebId'")
	public String putInjectionUnitWebIdToCache(String MachieName, String MachineTag) {
		log.info("--> HistorianService.putInjectionUnitWebIdToCache()");
		HashMap<String, String> replaceAttributes = new HashMap<String, String>();
		replaceAttributes.put(Constants.MACHINE_NAME, MachieName);
		replaceAttributes.put(Constants.MACHINE_TAG, MachineTag);
		return restTemplate
				.postForEntity(Constants.URL + "/batch", CommonUtil.replaceResource(resourceFile, replaceAttributes),
						BatchResponse.class)
				.getBody().getInjectionUnitAttributes().getContentWithHeader().getItemsWithHeader().get(0).getContent()
				.getItems().get(0).getWebId();
	}

}
