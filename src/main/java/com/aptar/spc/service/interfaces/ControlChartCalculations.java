package com.aptar.spc.service.interfaces;

public interface ControlChartCalculations<T> {

	public abstract T buildSubGroups(double[] array, int chunkSize);

	public abstract T getControlLimit();

	abstract T self();

	public abstract T loadAttirbute(double[] array);
}
