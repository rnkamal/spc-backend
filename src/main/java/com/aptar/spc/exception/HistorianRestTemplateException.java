package com.aptar.spc.exception;

import org.springframework.http.HttpStatus;

import lombok.Data;

@Data
public class HistorianRestTemplateException extends RuntimeException {

	private HttpStatus statusCode;
	private String error;

	public HistorianRestTemplateException(HttpStatus statusCode, String error) {
		super(error);
		this.statusCode = statusCode;
		this.error = error;
	}
}
