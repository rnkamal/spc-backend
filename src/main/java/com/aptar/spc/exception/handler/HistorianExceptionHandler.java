package com.aptar.spc.exception.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.aptar.spc.DTO.historian.response.HistorianErrorResponse;
import com.aptar.spc.exception.HistorianRestTemplateException;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class HistorianExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = HistorianRestTemplateException.class)
	ResponseEntity<HistorianErrorResponse> handleHistorianRestTemplateException(HistorianRestTemplateException ex,
			HttpServletRequest request) {
		log.error("An error :: ", ex.toString());
		return new ResponseEntity<>(new HistorianErrorResponse(ex, request.getRequestURI()), ex.getStatusCode());
	}
}
