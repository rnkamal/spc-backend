package com.aptar.spc.exception.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException.Unauthorized;

import com.aptar.spc.DTO.wrapper.SpcErrorResponse;
import com.aptar.spc.exception.ChartCalulationException;
import com.aptar.spc.exception.CommonException;
import com.aptar.spc.exception.DAOException;
import com.aptar.spc.exception.ExcelException;
import com.aptar.spc.exception.MapperException;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class AptarSPCExceptionHandler {

	@Autowired
	private ErrorAttributes errorAttributes;

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public SpcErrorResponse<?> handleValidationError(MethodArgumentNotValidException ex) {
		BindingResult bindingResult = ex.getBindingResult();
		String defaultMessage;
		try {
			FieldError fieldError = bindingResult.getFieldError();
			defaultMessage = fieldError.getDefaultMessage();
		} catch (Exception e) {
			ObjectError objectError = bindingResult.getGlobalError();
			defaultMessage = objectError.getDefaultMessage();
		}
		log.error("MethodArgumentNotValidException : " + defaultMessage);
		return new SpcErrorResponse<Object>(HttpStatus.BAD_REQUEST, defaultMessage);
	}

	@ExceptionHandler(value = javax.validation.ConstraintViolationException.class)
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public SpcErrorResponse<?> ConstraintViolationExceptionError(Exception ex) {
		log.error("ConstraintViolationException : " + ex.getLocalizedMessage());
		return new SpcErrorResponse<Object>(HttpStatus.BAD_REQUEST, ex.getMessage());
	}

	@ExceptionHandler(value = { Unauthorized.class })
	@ResponseStatus(code = HttpStatus.UNAUTHORIZED)
	public SpcErrorResponse<?> handleUnauthorizedException(Unauthorized ex) {
		log.error("Unauthorized Exception: ", ex.getMessage());
		return new SpcErrorResponse<Object>(HttpStatus.UNAUTHORIZED, ex.getMessage());
	}

	@ExceptionHandler(value = { DAOException.class })
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	public SpcErrorResponse<?> handleDAOException(DAOException ex) {
		log.error("DAO Exception: ", ex.getMessage());
		return new SpcErrorResponse<>(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
	}

	@ExceptionHandler(value = { MapperException.class })
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	public SpcErrorResponse<?> handleMapperException(MapperException ex) {
		log.error("Mapper Exception: ", ex);
		return new SpcErrorResponse<>(HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.toString());
	}

	@ExceptionHandler(value = { ExcelException.class })
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public SpcErrorResponse<?> handleExcelException(ExcelException ex) {
		log.error("Mapper Exception: ", ex.getMessage());
		return new SpcErrorResponse<>(HttpStatus.BAD_REQUEST, ex.getMessage());
	}

	@ExceptionHandler(value = { ChartCalulationException.class })
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public SpcErrorResponse<?> handleChartCalulationException(ChartCalulationException ex) {
		log.error("Mapper Exception: ", ex.getMessage());
		return new SpcErrorResponse<>(HttpStatus.BAD_REQUEST, ex.getMessage());
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
	@ResponseBody
	public SpcErrorResponse<?> getErrorMessage(HttpMediaTypeNotSupportedException ex) {
		String supported = "Supported content types: " + MediaType.toString(ex.getSupportedMediaTypes());
		return new SpcErrorResponse<Object>(HttpStatus.UNSUPPORTED_MEDIA_TYPE, supported);
	}

//	@ExceptionHandler(HttpMessageNotReadableException.class)
//	@ResponseStatus(HttpStatus.BAD_REQUEST)
//	public SpcErrorResponse<String> handleException(HttpMessageNotReadableException exception,
//			HttpServletRequest request) {
//		ServletWebRequest servletWebRequest = new ServletWebRequest(request);
//		Map<String, Object> errorAttributes = this.errorAttributes.getErrorAttributes(servletWebRequest, true);
//		errorAttributes.forEach((attribute, value) -> {
//			System.out.println(attribute + " --> " + value);
//		});
//
//		return new SpcErrorResponse<String>(HttpStatus.BAD_REQUEST, errorAttributes.get("message").toString());
//	}

	@ExceptionHandler(CommonException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public SpcErrorResponse<?> handleCommonException(CommonException ex) {
		log.error("CommonException Exception: ", ex);
		SpcErrorResponse<?> response = new SpcErrorResponse<>(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
		return response;
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public SpcErrorResponse<?> processAllError(Exception ex) {
		log.error("Unhandled Exception: ", ex);
		SpcErrorResponse<?> response = new SpcErrorResponse<>(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
		return response;
	}

}
