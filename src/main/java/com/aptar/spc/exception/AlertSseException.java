package com.aptar.spc.exception;

public class AlertSseException extends RuntimeException {

	public AlertSseException() {
		super();
	}

	public AlertSseException(String message, Throwable cause) {
		super(message, cause);
	}

	public AlertSseException(String message) {
		super(message);
	}

	public AlertSseException(Throwable cause) {
		super(cause);
	}
}
