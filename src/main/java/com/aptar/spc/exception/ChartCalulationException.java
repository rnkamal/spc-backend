package com.aptar.spc.exception;

public class ChartCalulationException extends RuntimeException {

	public ChartCalulationException() {
		super();
	}

	public ChartCalulationException(String message, Throwable cause) {
		super(message, cause);
	}

	public ChartCalulationException(String message) {
		super(message);
	}

	public ChartCalulationException(Throwable cause) {
		super(cause);
	}
}
