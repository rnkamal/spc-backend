package com.aptar.spc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aptar.spc.DTO.request.CombinationRevisionRequest;
import com.aptar.spc.DTO.response.CombinationRevisionParamResponse;
import com.aptar.spc.DTO.response.CombinationRevisionResponse;
import com.aptar.spc.DTO.wrapper.SpcSuccessResponse;
import com.aptar.spc.service.DAOservice.CombinationRevisionService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Validated
@Slf4j
@RequestMapping(value = "/combination", produces = MediaType.APPLICATION_JSON_VALUE)
public class CombinationRevisionController {

	@Autowired
	CombinationRevisionService combinationRevisionService;

	@PostMapping(value = "/revision/create", produces = MediaType.APPLICATION_JSON_VALUE)
	public CombinationRevisionResponse createNewCombinationRevision(
			@Valid @RequestBody CombinationRevisionRequest combinationRevisionRequest) {
		log.info("POST /combination/revision/create");

		return combinationRevisionService.createNewRevision(combinationRevisionRequest);
	}

	@GetMapping(value = "/{combinationId}/revision/active/param", produces = MediaType.APPLICATION_JSON_VALUE)
	public CombinationRevisionParamResponse getActiveCombinationRevision(
			@Valid @PathVariable("combinationId") Integer combinationId) {
		log.info("GET /combination" + combinationId + "/revision/active/param");

		return combinationRevisionService.getActiveCombinationRevisionNdParams(combinationId);
	}

	@GetMapping(value = "/{combinationId}/revision/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CombinationRevisionResponse> getAllCombinationRevision(
			@Valid @PathVariable("combinationId") Integer combinationId) {
		log.info("GET /combination/" + combinationId + "/revision/all");

		return combinationRevisionService.getAllCombinationRevision(combinationId);
	}

	@PutMapping(value = "/revision/{combinationRevisionId}/activate", produces = MediaType.APPLICATION_JSON_VALUE)
	public SpcSuccessResponse activateCombinationRevisionById(
			@Valid @PathVariable("combinationRevisionId") Integer combinationRevisionId) {
		log.info("Put /combination/revision/{combinationRevisionId}/activate");

		return combinationRevisionService.activateCombinationRevision(combinationRevisionId);
	}

}
