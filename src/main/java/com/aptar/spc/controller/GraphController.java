package com.aptar.spc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aptar.spc.DTO.response.GraphResponse;
import com.aptar.spc.service.DAOservice.GraphService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping(value = "/graph")
public class GraphController {

	@Autowired
	GraphService graphService;

	@GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
	@Cacheable(value = "GraphList")
	public List<GraphResponse> getAllGraph() {
		log.info("GET /graph/all");
		return graphService.getAllGraphs();
	}

	@GetMapping(value = "/{id}/name", produces = MediaType.TEXT_PLAIN_VALUE)
	@Cacheable(value = "GraphNameById", key = "'GraphId'+#id")
	public String getGraphName(@Valid @PathVariable("id") Integer id) {
		log.info("GET /graph/" + id + "/name");
		return graphService.getGraphName(id);
	}

}
