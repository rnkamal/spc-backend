package com.aptar.spc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aptar.spc.DTO.response.PoRevisionParamDropdownResponse;
import com.aptar.spc.DTO.response.PoRevisionParamResponse;
import com.aptar.spc.DTO.wrapper.SpcSuccessResponse;
import com.aptar.spc.service.DAOservice.PoRevisionParamService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Validated
@Slf4j
@RequestMapping(value = "/combination/revision")
public class PoCombinationRevisionParamController {

	@Autowired
	PoRevisionParamService poRevisionParamService;

	@PutMapping(value = "/poParams/{revisionParamId}/deactivateSpc")
	public SpcSuccessResponse<String> deactivatePoCombinationRevisionParamSpc(
			@PathVariable("revisionParamId") Integer revisionParamId) {
		log.info("Get /combination/revision/params/" + revisionParamId + "/deactivateSpc");
		return poRevisionParamService.deactivateSpcAlertByRevisionId(revisionParamId);
	}

	@GetMapping(value = "/{poCombinationRevisionId}/poParams/activeSpc", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PoRevisionParamResponse> getActiveSpcPoCombinationRevisionParamsById(
			@Valid @PathVariable("poCombinationRevisionId") Integer poCombinationRevisionId) {
		log.info("GET /combination/revision/{" + poCombinationRevisionId + "}/poParams/activeSpc");

		return poRevisionParamService.getAllSpcEnablePoCombinationRevisionParams(poCombinationRevisionId);
	}

	@GetMapping(value = "/poParams/{revision-param-id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PoRevisionParamResponse> getSpcEnablePoRevisionParamById(
			@Valid @PathVariable("revision-param-id") List<Integer> combinationRevisionParamId) {
		log.info("Get /combination/revision/poParams/" + combinationRevisionParamId);
		return poRevisionParamService.getPoRevisionParamById(combinationRevisionParamId);
	}

	@GetMapping(value = "/{PoCombinationRevisionId}/poParamsDropDown/activeSpc", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PoRevisionParamDropdownResponse> getPoCombinationRevisionParamsDropDownById(
			@Valid @PathVariable("PoCombinationRevisionId") Integer poCombinationRevisionId) {
		log.info("GET /combination/revision/{" + poCombinationRevisionId + "}/poParamsDropDown/activeSpc");

		return poRevisionParamService.getAllSpcEnablePoCombinationRevisionParamsDropdown(poCombinationRevisionId);
	}

}
