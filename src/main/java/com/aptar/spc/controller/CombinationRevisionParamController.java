package com.aptar.spc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.aptar.spc.DTO.request.RevisionParamRequest;
import com.aptar.spc.DTO.response.Attribute;
import com.aptar.spc.DTO.response.ControlChartResponse;
import com.aptar.spc.DTO.response.RevisionParamResponse;
import com.aptar.spc.DTO.wrapper.SpcSuccessResponse;
import com.aptar.spc.service.DAOservice.RevisionParamService;
import com.aptar.spc.service.DAOservice.UploadExcelService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Validated
@Slf4j
@RequestMapping(value = "/combination/revision", produces = MediaType.APPLICATION_JSON_VALUE)
public class CombinationRevisionParamController {

	@Autowired
	UploadExcelService uploadExcelService;

	@Autowired
	RevisionParamService revisionParamService;

	@GetMapping(value = "/{combinationRevisionId}/param", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<RevisionParamResponse> getCombinationRevisionParamsById(
			@Valid @PathVariable("combinationRevisionId") Integer combinationRevisionId) {
		log.info("GET /combination/revision/{combinationRevisionId}/param");

		return revisionParamService.getAllCombinationRevisionParams(combinationRevisionId);
	}

	@PostMapping(value = { "/params/{control-chart-type}/upload-excel/control-limit",
			"/params/{control-chart-type}/{sub-group}/upload-excel/control-limit" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ControlChartResponse uploadExcel(@Valid @RequestParam("file") MultipartFile file,
			@PathVariable(required = false, name = "sub-group") Integer subGroup,
			@PathVariable("control-chart-type") String controlChartType) {
		log.info("Post /combination/revision/params/" + controlChartType + "/upload-excel/control-limit");
		return revisionParamService.calculateControlLimit(file, subGroup, controlChartType);
	}

	@PostMapping(value = { "/params/{control-chart-type}/{excel-id}/recalculate-control-limit",
			"/params/{control-chart-type}/{sub-group}/{excel-id}/recalculate-control-limit" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ControlChartResponse recalculateControlLimit(@Valid @RequestBody List<Attribute> controlChartRequest,
			@PathVariable(required = false, name = "sub-group") Integer subGroup,
			@PathVariable("excel-id") Integer excelId, @PathVariable("control-chart-type") String controlChartType) {
		log.info("Post /combination/revision/params/" + controlChartType + "/recalculate-control-limit");
		return revisionParamService.reCalculateControlLimit(controlChartRequest, subGroup, excelId, controlChartType);
	}

	@GetMapping(value = "/params/{revision-param-id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public RevisionParamResponse getRevisionParam(
			@Valid @PathVariable("revision-param-id") Integer combinationRevisionParamId) {
		log.info("Get /combination/revision/params/" + combinationRevisionParamId);
		return revisionParamService.getRevisionParamById(combinationRevisionParamId);
	}

	@SuppressWarnings("rawtypes")
	@PutMapping(value = "/params/update", produces = MediaType.APPLICATION_JSON_VALUE)
	public SpcSuccessResponse updateRevisionParam(@Valid @RequestBody RevisionParamRequest revisionParamRequest) {
		log.info("PUT /combination/revision/params/update");
		return revisionParamService.updateRevisionParamById(revisionParamRequest);
	}

}
