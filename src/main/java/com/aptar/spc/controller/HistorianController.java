package com.aptar.spc.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aptar.spc.DTO.historian.request.ChartRecordsByTimeRequest;
import com.aptar.spc.DTO.historian.request.ChartRecordsByTimeStampRequest;
import com.aptar.spc.DTO.historian.request.ChartWebIdRequest;
import com.aptar.spc.DTO.historian.request.InjectionUnitWebIdRequest;
import com.aptar.spc.DTO.historian.request.MachineParamterRequest;
import com.aptar.spc.DTO.historian.response.ItemListChartResponse;
import com.aptar.spc.service.historian.ChartAttributeService;
import com.aptar.spc.service.historian.ChartAttributeValueService;
import com.aptar.spc.service.historian.ChartSubAttributeValueService;
import com.aptar.spc.service.historian.InjectionUnitService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Validated
@Slf4j
@RequestMapping(value = "/historian")
public class HistorianController {

	@Autowired
	InjectionUnitService injectionUnitService;

	@Autowired
	ChartAttributeService chartAttributeService;

	@Autowired
	ChartAttributeValueService chartAttributeValueService;

	@Autowired
	ChartSubAttributeValueService chartSubAttributeValueService;

	@PostMapping(value = "/injectionUnitwebId")
	public String getInjectionUnitWebId(@Valid @RequestBody InjectionUnitWebIdRequest injectionUnitWebIdRequest) {
		log.info("Post /historian/injectionUnitwebId");
		return injectionUnitService.addInjectionUnitWebIdToCache(injectionUnitWebIdRequest.getMachineName(),
				injectionUnitWebIdRequest.getMachineTag());
	}

	@PostMapping(value = "/chartWebId")
	public String getChartWebID(@Valid @RequestBody ChartWebIdRequest chartWebIdRequest) {
		log.info("Post /historian/chartWebId");
		return chartAttributeService.addChartWebIdToCache(chartWebIdRequest.getMachineName(),
				chartWebIdRequest.getMachineTag(), chartWebIdRequest.getChart());
	}

	@PostMapping(value = "/chartRecordsByTime")
	public ItemListChartResponse getChartRecordsByTime(
			@Valid @RequestBody ChartRecordsByTimeRequest chartRecordsByTimeRequest) {
		log.info("Post /historian/chartRecordsByTime");
		return chartAttributeValueService.getChartValuesByTime(chartRecordsByTimeRequest.getMachineName(),
				chartRecordsByTimeRequest.getMachineParamaterTag(), chartRecordsByTimeRequest.getChart(),
				chartRecordsByTimeRequest.getTime(), chartRecordsByTimeRequest.getMaxCount());
	}

	@PostMapping(value = "/chartRecordAfterTimeStamp")
	public ItemListChartResponse getChartRecordsByStartTimeStamp(
			@Valid @RequestBody ChartRecordsByTimeStampRequest chartRecordsByTimeStampRequest) {
		log.info("Post /historian/chartRecordAfterTimeStamp");
		return chartAttributeValueService.getChartValuesAfterTimeStamp(chartRecordsByTimeStampRequest.getMachineName(),
				chartRecordsByTimeStampRequest.getMachineParamaterTag(), chartRecordsByTimeStampRequest.getChart(),
				chartRecordsByTimeStampRequest.getStartTimeStamp(), chartRecordsByTimeStampRequest.getMaxCount());
	}

	@PostMapping(value = "/machineParamter")
	public void setMachineParamterValue(@Valid @RequestBody MachineParamterRequest machineParamterRequest) {
		log.info("Post /machineParamter");
		chartSubAttributeValueService.setSubAttributeValue(machineParamterRequest.getMachineName(),
				machineParamterRequest.getMachineTag(), machineParamterRequest.getChart(),
				machineParamterRequest.getParamter(), machineParamterRequest.getValue());
	}
}
