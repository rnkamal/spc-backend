package com.aptar.spc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aptar.spc.DTO.request.MachineRequest;
import com.aptar.spc.DTO.response.MachineWithSpcResponse;
import com.aptar.spc.service.DAOservice.MachineService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping(value = "/machine", produces = MediaType.APPLICATION_JSON_VALUE)
public class MachineController {

	@Autowired
	MachineService machineService;

	@PostMapping(value = "/spcstatus")
	public List<MachineWithSpcResponse> getAllMachineWithSpcStatus(@Valid @RequestBody MachineRequest machineRequest) {
		log.info("GET /machine/spcstatus");
		return machineService.getAllMachineWithSpcStatus(machineRequest.getSite(),
				machineRequest.getFunctionalLocation());
	}

}
