//package com.aptar.spc.controller;
//
//import java.util.Collections;
//
//import javax.servlet.http.HttpServletResponse;
//import javax.validation.Valid;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.aptar.mes.security.TokenProvider;
//import com.aptar.spc.DTO.AuthenticationResponse;
//import com.aptar.spc.DTO.LoginRequestDTO;
//
//@RestController
//@RequestMapping("/authenticate")
//public class AuthenticationController {
//
//	@Autowired
//	private TokenProvider tokenProvider;
//
//	@Autowired
//	private AuthenticationManager authenticationManager;
//
//	@PostMapping(value = "/token")
//	public ResponseEntity<?> tokenAuthorize(@Valid @RequestBody final LoginRequestDTO loginVM,
//			final HttpServletResponse response) {
//		try {
//			Authentication authentication = authenticateFromLogin(loginVM);
//			String jwt = tokenProvider.createTokenFromAuthentication(authentication);
//			response.addHeader(SecurityConstants.JWT_AUTHORIZATION_HEADER, "Bearer " + jwt);
//			return ResponseEntity.ok(new AuthenticationResponse(jwt));
//		} catch (AuthenticationException exception) {
//			return new ResponseEntity<>(
//					Collections.singletonMap("AuthenticationException", exception.getLocalizedMessage()),
//					HttpStatus.UNAUTHORIZED);
//		}
//	}
//
//	private Authentication authenticateFromLogin(final LoginRequestDTO loginVM) throws AuthenticationException {
//		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
//				loginVM.getUsername(), loginVM.getPassword());
//		Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
//		SecurityContextHolder.getContext().setAuthentication(authentication);
//		return authentication;
//	}
//}
