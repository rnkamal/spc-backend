package com.aptar.spc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aptar.spc.DTO.response.SpcRuleResponse;
import com.aptar.spc.service.DAOservice.SpcRuleService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping(value = "/spc-rule", produces = MediaType.APPLICATION_JSON_VALUE)
public class SpcRuleController {

	@Autowired
	SpcRuleService spcRuleService;

	@GetMapping(value = "/all")
	public List<SpcRuleResponse> getAllSpcRule() {
		log.info("GET /spc-rule/all");
		return spcRuleService.getAllSpcRules();
	}

}
