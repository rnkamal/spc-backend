package com.aptar.spc.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.aptar.spc.DTO.request.AlertFromHistorianRequest;
import com.aptar.spc.DTO.response.AlertResponse;
import com.aptar.spc.service.DAOservice.SpcAlertService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Validated
@Slf4j
@RequestMapping(value = "/alert")
public class AlertController {

	@Autowired
	SpcAlertService spcAlertService;

	@PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
	public AlertResponse createNewSpcAlert(@Valid @RequestBody AlertFromHistorianRequest alertFromHistorianRequest) {
		log.info("POST /alert/create");

		return spcAlertService.createAlert(alertFromHistorianRequest);
	}

	@GetMapping(value = "/get", consumes = MediaType.ALL_VALUE)
	public SseEmitter subscribeAlert() {
		return spcAlertService.subscribeAlert("vicky");
	}

}
