package com.aptar.spc.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aptar.spc.DTO.request.CombinationRequest;
import com.aptar.spc.DTO.response.CombinationResponse;
import com.aptar.spc.service.DAOservice.CombinationService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Validated
@Slf4j
@RequestMapping(value = "/combination", produces = MediaType.APPLICATION_JSON_VALUE)
public class CombinationController {

	@Autowired
	CombinationService combinationService;

	@PostMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
	public CombinationResponse getAllCombination(@Valid @RequestBody CombinationRequest combinationRequest) {
		log.info("GET /combination/list");

		return combinationService.getAllCombination(combinationRequest.getSite(),
				combinationRequest.getFunctionalLocation());
	}

	@GetMapping(value = "/{combination-id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public CombinationResponse getCombinationById(@Valid @PathVariable("combination-id") Integer combinationId) {
		log.info("GET /combination/" + combinationId);

		return combinationService.getCombinationById(combinationId);
	}

	@GetMapping(value = "/{combination-id}/spcAlert", produces = MediaType.APPLICATION_JSON_VALUE)
	public CombinationResponse getCombinationByIdWithSpcAlert(
			@Valid @PathVariable("combination-id") Integer combinationId) {
		log.info("GET /combination/" + combinationId + "/spcAlert");

		return combinationService.getCombinationWithSpcStatusById(combinationId);
	}

}
