package com.aptar.spc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aptar.spc.DTO.response.FunctionalLocResponse;
import com.aptar.spc.DTO.response.SiteResponse;
import com.aptar.spc.service.DAOservice.UserService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping(value = "/sites")
	public List<SiteResponse> getAllDepartments() {
		log.info("GET /user/sites");
		return userService.getSite();
	}

	@GetMapping(value = "/sites/{id}/functional-loc")
	public List<FunctionalLocResponse> getFunctLocBySiteId(@Valid @PathVariable("id") Integer siteId) {
		log.info("GET /user/sites");
		return userService.getFunctionLocBySiteId(siteId);
	}

}
