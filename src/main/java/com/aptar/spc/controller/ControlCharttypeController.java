package com.aptar.spc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aptar.spc.DTO.response.ControlChartTypeResponse;
import com.aptar.spc.DTO.response.GraphResponse;
import com.aptar.spc.service.DAOservice.ContolChartTypeService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping(value = "/control-chart", produces = MediaType.APPLICATION_JSON_VALUE)
public class ControlCharttypeController {

	@Autowired
	ContolChartTypeService controlChartService;

	@GetMapping(value = "/all")
	public List<ControlChartTypeResponse> getAllControlChart() {
		log.info("GET /control-chart/all");
		return controlChartService.getAllContolChartType();
	}

	@GetMapping(value = "/{ControlChartId}/graph")
	public List<GraphResponse> getAllGraphByControlChart(
			@Valid @PathVariable("ControlChartId") Integer controlChartId) {
		log.info("GET /control-chart/" + controlChartId + "/all");
		return controlChartService.getAllGraphByControlChart(controlChartId);
	}

}
