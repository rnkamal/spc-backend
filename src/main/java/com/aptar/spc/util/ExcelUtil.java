package com.aptar.spc.util;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import com.aptar.spc.exception.ExcelException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExcelUtil {
	private static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	private static String SHEET = "values";
	private static InputStream file;

	public static ExcelUtil ExcelUtil(InputStream inputStream) {
		file = inputStream;
		return new ExcelUtil();
	}

	public static ExcelUtil hasExcelFormat(MultipartFile file) {
		if (!TYPE.equals(file.getContentType())) {
			log.error("Only application/vnd.ms-excel is allowable");
			throw new ExcelException("Only application/vnd.ms-excel is allowable");
		}
		try {
			return ExcelUtil(file.getInputStream());
		} catch (Exception e) {
			log.error(e.getCause().toString());
			throw new ExcelException(e.getCause());
		}
	}

	public List<Double> parseExcel() {
		try {
			Workbook workbook = new XSSFWorkbook(file);
			Sheet sheet = workbook.getSheet(SHEET);
			Iterator<Row> rows = sheet.iterator();
			List<Double> excelValues = new ArrayList<Double>();
			int rowNumber = 0;
			while (rows.hasNext()) {
				Row currentRow = rows.next();
				// skip header
				if (rowNumber == 0) {
					rowNumber++;
					continue;
				}
				Iterator<Cell> cellsInRow = currentRow.iterator();
				int cellIdx = 0;
				while (cellsInRow.hasNext()) {
					Cell currentCell = cellsInRow.next();

					switch (cellIdx) {
					case 0:
						try {
							excelValues.add((Double) currentCell.getNumericCellValue());
						} catch (Exception e) {
							log.error("Invalid data format at : " + currentCell.getColumnIndex());
							throw new ExcelException("Invalid data format at : " + currentCell.getColumnIndex());
						}
						break;
					default:
						break;
					}

					cellIdx++;
				}
			}
			workbook.close();
			return excelValues;
		} catch (Exception e) {
			log.error("fail to parse Excel file : " + e.getMessage());
			throw new ExcelException("fail to parse Excel file : " + e.getMessage());
		}
	}
}
