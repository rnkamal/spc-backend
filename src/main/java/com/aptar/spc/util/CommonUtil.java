package com.aptar.spc.util;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import org.springframework.core.io.Resource;

import com.aptar.spc.exception.CommonException;
import com.google.common.io.CharStreams;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CommonUtil {

	public static String replaceResource(Resource resource, HashMap<String, String> replacabelValue) {
		try {
			InputStream resourcee = resource.getInputStream();
			final Reader reader = new InputStreamReader(resourcee);
			String replacedResource = CharStreams.toString(reader);
			for (Map.Entry<String, String> entry : replacabelValue.entrySet()) {
				replacedResource = replacedResource.replaceAll(entry.getKey(), entry.getValue());
			}
			return replacedResource;
		} catch (Exception e) {
			log.error(e.getCause().toString());
			throw new CommonException(e.getCause());
		}
	}

}
