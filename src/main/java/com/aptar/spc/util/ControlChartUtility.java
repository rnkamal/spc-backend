package com.aptar.spc.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

import com.aptar.spc.DTO.response.Attribute;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ControlChartUtility {

	public static HashMap<Integer, Object> splitArray(double[] array, int chunkSize) {
		log.info("--> ControlChartUtility.splitArray()");
		int numOfChunks = (int) Math.ceil((double) array.length / chunkSize);
		HashMap<Integer, Object> output = new HashMap<Integer, Object>();

		for (int i = 0; i < numOfChunks; ++i) {
			int start = i * chunkSize;
			int length = Math.min(array.length - start, chunkSize);

			double[] temp = new double[length];
			System.arraycopy(array, start, temp, 0, length);
			output.put(i, temp);
		}

		return output;
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	public static List<Double> splitControlLimitRegion(int region, double controlLimit, double mean, int roundOff) {

		double difference = (Math.max(controlLimit, mean) - Math.min(controlLimit, mean));
		double partition = difference / region;
		List<Double> splitedControlLimt = new ArrayList<>();
		for (int i = 1; i <= region; i++) {
			mean += partition;
			splitedControlLimt.add(round(mean, roundOff));
		}
		return splitedControlLimt;
	}

	public static List<Attribute> checkAttributeListWithControlLimit(double[] attributeValue, double ucl, double lcl) {
		Double[] doubleAttributeValue = ArrayUtils.toObject(attributeValue);
		List<Attribute> attributeList = new ArrayList<>();
		Arrays.asList(doubleAttributeValue).forEach(a -> {
			attributeList.add(Attribute.builder().attibuteValue(a)
					.controlLimitStatus(checkAttributeWithControlLimit(a, ucl, lcl)).build());
		});
		return attributeList;
	}

	public static boolean checkAttributeWithControlLimit(double s, double ucl, double lcl) {
		boolean status = false;
		if (s > ucl || s > lcl) {
			status = true;
		}
		return status;
	}

}
