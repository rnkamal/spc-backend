package com.aptar.spc;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.aptar.spc.constants.Constants;
import com.aptar.spc.exception.handler.HistorianRestTemplateErrorHandler;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@EnableCaching
@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "AptarSPC", version = "v1", description = "spc-public-api"))
public class AptarSpcApplication {

	public static void main(String[] args) {
		SpringApplication.run(AptarSpcApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder)
			throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
		SSLContext sslContext = SSLContextBuilder.create().loadTrustMaterial(acceptingTrustStrategy).build();
		HttpClient client = HttpClients.custom().setSSLContext(sslContext).build();
		return restTemplateBuilder.errorHandler(new HistorianRestTemplateErrorHandler())
				.requestFactory(() -> new HttpComponentsClientHttpRequestFactory(client))
				.basicAuthentication(Constants.USERNAME, Constants.PASSWORD).build();
	}

}
