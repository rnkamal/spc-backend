package com.aptar.spc.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * The persistent class for the production_order database table.
 * 
 */
@Data
@ToString
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "production_order")
@NamedQuery(name = "ProductionOrder.findAll", query = "SELECT p FROM ProductionOrder p")
public class ProductionOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "PRODUCTION_ORDER_ID_GENERATOR")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRODUCTION_ORDER_ID_GENERATOR")
	@Column(unique = true, nullable = false)
	private Integer id;

	@CreatedBy
	@Column(nullable = false, updatable = false)
	private String createdby;

	@CreatedDate
	@Column(nullable = false, updatable = false)
	private Timestamp createdon;

	@Column(name = "production_order", nullable = false, length = 55)
	private String productionOrder;

	@LastModifiedBy
	private String updatedby;

	@LastModifiedDate
	private Timestamp updatedon;

	// bi-directional many-to-one association to Combination
	@OneToMany(mappedBy = "productionOrder")
	private List<Combination> combinations;

	// bi-directional many-to-one association to PoCombinationRevisionParameterRef
	@OneToMany(mappedBy = "productionOrder")
	private List<PoCombinationRevisionParameterRef> poCombinationRevisionParameterRefs;

	// bi-directional many-to-one association to MachineGroup
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "machine_group_id")
	private MachineGroup machineGroup;

	// bi-directional many-to-one association to Machine
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "machine_id")
	private Machine machine;

	// bi-directional many-to-one association to Site
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "site_id")
	private Site site;

}