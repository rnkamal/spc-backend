package com.aptar.spc.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * The persistent class for the combinations database table.
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "combinations")
@NamedQuery(name = "Combination.findAll", query = "SELECT c FROM Combination c")
public class Combination implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "COMBINATIONS_ID_GENERATOR")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMBINATIONS_ID_GENERATOR")
	@Column(unique = true, nullable = false)
	private Integer id;

	@CreatedBy
	@Column(nullable = false, updatable = false)
	@NotBlank(message = "createdby is mandatory")
	private String createdby;

	@CreatedDate
	@Column(nullable = false, updatable = false)
	private Timestamp createdon;

	@LastModifiedBy
	private String updatedby;

	@LastModifiedDate
	private Timestamp updatedon;

	private String product;

	// bi-directional many-to-one association to CombinationAttributeValue
	@OneToMany(mappedBy = "combination")
	private List<CombinationAttributeValue> combinationAttributeValues;

	// bi-directional many-to-one association to CombinationRevision
	@OneToMany(mappedBy = "combination")
	private List<CombinationRevision> combinationRevisions;

	// bi-directional many-to-one association to CombinationTemplate
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "combinationtemplate_id", nullable = false)
	private CombinationTemplate combinationTemplate;

	// bi-directional many-to-one association to Machine
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "machine_id", nullable = false)
	private Machine machine;

	// bi-directional many-to-one association to ProductionOrder
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "po_id")
	private ProductionOrder productionOrder;

}