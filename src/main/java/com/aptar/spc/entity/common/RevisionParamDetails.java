package com.aptar.spc.entity.common;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RevisionParamDetails {

	private Integer controlChartTypeId;
	private Integer graphId;
	private String uom;
	private double stdValue;
	private double lsl;
	private double usl;
	private String calculationType;
	@JsonProperty("points")
	private String automaticCalcPoint;
	private List<GraphDetails> graphDetails;
}
