package com.aptar.spc.entity.common;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GraphDetails {

	private Integer graphId;
	private Integer[] graphRules;
	private double[] lcl;
	private double[] ucl;
	private double mean;
	private double average;
}
