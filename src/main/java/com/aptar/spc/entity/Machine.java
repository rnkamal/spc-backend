package com.aptar.spc.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;
import lombok.ToString;

/**
 * The persistent class for the machines database table.
 * 
 */
@Data
@ToString
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "machines")
@NamedQuery(name = "Machine.findAll", query = "SELECT m FROM Machine m")
public class Machine implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "MACHINES_ID_GENERATOR")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MACHINES_ID_GENERATOR")
	@Column(unique = true, nullable = false)
	private Integer id;

	@Column(nullable = false)
	private Boolean active;

	@CreatedBy
	@Column(nullable = false, updatable = false)
	private String createdby;

	@CreatedDate
	@Column(nullable = false, updatable = false)
	private Timestamp createdon;

	@Column(length = 155)
	private String description;

	@Column(nullable = false, length = 100)
	private String name;

	@LastModifiedBy
	private String updatedby;

	@LastModifiedDate
	private Timestamp updatedon;

	// bi-directional many-to-one association to Combination
	@OneToMany(mappedBy = "machine")
	private List<Combination> combinations;

	// bi-directional many-to-one association to ControlChartType
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "control_chart_type_id")
	private ControlChartType controlChartType;

	// bi-directional many-to-one association to Department
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "department_id")
	private Department department;

	// bi-directional many-to-one association to FunctionalLoc
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fucntional_loc_id")
	private FunctionalLoc functionalLoc;

	// bi-directional many-to-one association to Graph
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "graph_id")
	private Graph graph;

	// bi-directional many-to-one association to MachineGroup
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "machinegroup_id")
	private MachineGroup machineGroup;

	// bi-directional many-to-one association to MachineType
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "macine_type_id")
	private MachineType machineType;

	// bi-directional many-to-one association to ProductionOrder
	@OneToMany(mappedBy = "machine")
	private List<ProductionOrder> productionOrders;

}