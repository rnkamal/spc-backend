package com.aptar.spc.entity.EntityListener;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;

public class AuditorAwareImpl implements AuditorAware<String> {

//	@Autowired
//	LoginDetails loginDetails;
//
//	@Override
//	public Optional<String> getCurrentAuditor() {
//		return loginDetails.getLoginUserName();
//	}

	@Override
	public Optional<String> getCurrentAuditor() {
		return Optional.of("vicky");
		// Can use Spring Security to return currently logged in user
		// return ((User)
		// SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()
	}
}