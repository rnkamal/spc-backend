//package com.aptar.spc.entity.EntityListener;
//
//import java.sql.Timestamp;
//
//import javax.persistence.Column;
//import javax.persistence.EntityListeners;
//import javax.persistence.MappedSuperclass;
//
//import org.springframework.data.annotation.CreatedBy;
//import org.springframework.data.annotation.CreatedDate;
//import org.springframework.data.annotation.LastModifiedBy;
//import org.springframework.data.annotation.LastModifiedDate;
//import org.springframework.data.jpa.domain.support.AuditingEntityListener;
//
//import lombok.AccessLevel;
//import lombok.Getter;
//import lombok.Setter;
//
//@Getter(AccessLevel.PUBLIC)
//@Setter(AccessLevel.PUBLIC)
//@MappedSuperclass
//@EntityListeners(AuditingEntityListener.class)
//public class Auditable<U> {
//	@CreatedBy
//	@Column(name = "createdby")
//	private U createdby;
//
//	@CreatedDate
//	@Column(name = "createdon")
//	private Timestamp createdon;
//
//	@LastModifiedBy
//	@Column(name = "updatedby")
//	private U updatedby;
//
//	@LastModifiedDate
//	@Column(name = "updateon")
//	private Timestamp updateon;
//}