//package com.aptar.spc.entity.EntityListener;
//
//import java.security.Principal;
//import java.util.Optional;
//
//import javax.servlet.http.HttpServletRequest;
//
//import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
//import org.keycloak.representations.AccessToken;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//@Component
//public class LoginDetails {
//
//	@Autowired
//	private HttpServletRequest request;
//
//	public Optional<String> getLoginUserName() {
//		AccessToken accessToken = this.getKeycloakToken(request.getUserPrincipal());
//		String userName = accessToken.getPreferredUsername();
//		return Optional.ofNullable(userName);
//	}
//
//	private AccessToken getKeycloakToken(Principal principal) {
//		KeycloakAuthenticationToken keycloakAuthenticationToken = (KeycloakAuthenticationToken) principal;
//		return keycloakAuthenticationToken.getAccount().getKeycloakSecurityContext().getToken();
//	}
//
//}
