package com.aptar.spc.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.aptar.spc.DTO.response.Approvers;
import com.aptar.spc.entity.common.RevisionParamDetails;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * The persistent class for the po_combination_revision_parameter_ref database
 * table.
 * 
 */
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Data
@ToString
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "po_combination_revision_parameter_ref")
@NamedQuery(name = "PoCombinationRevisionParameterRef.findAll", query = "SELECT p FROM PoCombinationRevisionParameterRef p")
public class PoCombinationRevisionParameterRef implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "PO_COMBINATION_REVISION_PARAMETER_REF_ID_GENERATOR")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PO_COMBINATION_REVISION_PARAMETER_REF_ID_GENERATOR")
	@Column(unique = true, nullable = false)
	private Integer id;

	@Column(nullable = false)
	private Boolean active;

	private Approvers approvers;

	@Column(name = "combination_revision_id", nullable = false)
	private Integer combinationRevisionId;

	@CreatedBy
	@Column(nullable = false, updatable = false)
	private String createdby;

	@CreatedDate
	@Column(nullable = false, updatable = false)
	private Timestamp createdon;

	@Column(name = "spc", nullable = false)
	private Boolean spc;

	@Column(name = "alert_status", nullable = false)
	private Boolean alertStatus;

	@Column(name = "parameter_id", nullable = false)
	private Integer parameterId;

	@Column(name = "po_endedon", nullable = false)
	private Timestamp poEndedon;

	@Column(name = "po_startedon", nullable = false)
	private Timestamp poStartedon;

	@Column(nullable = false)
	private Integer status;

	@LastModifiedBy
	private String updatedby;

	@LastModifiedDate
	private Timestamp updatedon;

	@Type(type = "jsonb")
	@Column(columnDefinition = "jsonb", name = "revision_param_details", nullable = false)
	private RevisionParamDetails revisionParamDetails;

	// bi-directional many-to-one association to ProductionOrder
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "po_id", nullable = false)
	private ProductionOrder productionOrder;

	// bi-directional many-to-one association to PoParameterApprovalStatus
	@OneToMany(mappedBy = "poCombinationRevisionParameterRef")
	private List<PoParameterApprovalStatus> poParameterApprovalStatuses;

	// bi-directional many-to-one association to SpcAlert
	@OneToMany(mappedBy = "poCombinationRevisionParameterRefInAlert")
	private List<SpcAlert> spcAlerts;
}