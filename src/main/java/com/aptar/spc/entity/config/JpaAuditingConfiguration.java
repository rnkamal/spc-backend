package com.aptar.spc.entity.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.aptar.spc.entity.EntityListener.AuditorAwareImpl;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class JpaAuditingConfiguration {

	@Bean
	public AuditorAware<String> auditorProvider() {

		/*
		 * if you are using spring security, you can get the currently logged username
		 * with following code segment.
		 * SecurityContextHolder.getContext().getAuthentication().getName()
		 */
//        return () –> Optional.ofNullable(1));
		return new AuditorAwareImpl();
	}
}
