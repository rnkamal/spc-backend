package com.aptar.spc.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * The persistent class for the po_parameter_approval_status database table.
 * 
 */
@Data
@ToString
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "po_parameter_approval_status")
@NamedQuery(name = "PoParameterApprovalStatus.findAll", query = "SELECT p FROM PoParameterApprovalStatus p")
public class PoParameterApprovalStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "PO_PARAMETER_APPROVAL_STATUS_ID_GENERATOR")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PO_PARAMETER_APPROVAL_STATUS_ID_GENERATOR")
	@Column(unique = true, nullable = false)
	private Integer id;

	private Timestamp approvedon;

	@Column(name = "approver_id")
	private Integer approverId;

	@CreatedBy
	@Column(nullable = false, updatable = false)
	private String createdby;

	@CreatedDate
	@Column(nullable = false, updatable = false)
	private Timestamp createdon;

	@Column(length = 2147483647)
	private String reason;

	@Column(name = "role_id", nullable = false)
	private Integer roleId;

	@Column(name = "status_id", nullable = false)
	private Integer statusId;

	// bi-directional many-to-one association to PoCombinationRevisionParameterRef
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "po_parameter_id", nullable = false)
	private PoCombinationRevisionParameterRef poCombinationRevisionParameterRef;

}