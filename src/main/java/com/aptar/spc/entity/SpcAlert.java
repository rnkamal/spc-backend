package com.aptar.spc.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "spc_alert")
@NamedQuery(name = "SpcAlert.findAll", query = "SELECT s FROM SpcAlert s")
public class SpcAlert implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SPC_ALERT_ID_GENERATOR")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SPC_ALERT_ID_GENERATOR")
	@Column(unique = true, nullable = false)
	private Integer id;

	@Column(name = "alert_value", length = 55)
	private String alertValue;

	@Column(name = "production_order", nullable = false, length = 55)
	private String productionOrder;

	@Column(length = 55)
	private String graph;

	@Column(nullable = false, length = 55)
	private String machine;

	@Column(length = 55)
	private String path;

	@Column(nullable = false, length = 55)
	private String site;

	@Column(nullable = false, length = 55)
	private String tag;

	@Column(nullable = false)
	private Timestamp timestamp;

	@Column(name = "violated_rule", length = 55)
	private String violatedRule;

	// bi-directional many-to-one association to PoCombinationRevisionParameterRef
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "po_combination_revision_param_id", nullable = false)
	private PoCombinationRevisionParameterRef poCombinationRevisionParameterRefInAlert;

}
