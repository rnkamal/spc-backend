package com.aptar.spc.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * The persistent class for the combination_revisions database table.
 * 
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "combination_revisions")
@NamedQuery(name = "CombinationRevision.findAll", query = "SELECT c FROM CombinationRevision c")
public class CombinationRevision implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "COMBINATION_REVISIONS_ID_GENERATOR")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMBINATION_REVISIONS_ID_GENERATOR")
	@Column(unique = true, nullable = false)
	private Integer id;

	@Column(nullable = false)
	private Boolean active;

	@CreatedBy
	@Column(nullable = false, updatable = false)
	private String createdby;

	@CreatedDate
	@Column(nullable = false, updatable = false)
	private Timestamp createdon;

	@Column(name = "is_po_subrevision", nullable = false)
	private Boolean isPoSubrevision;

	@Column(nullable = false)
	private Integer revison;

	@LastModifiedBy
	private String updatedby;

	@LastModifiedDate
	private Timestamp updateon;

	@Size(max = 55, message = "reason size must be less than 55")
	private String reason;

	// bi-directional many-to-one association to CombinationRevisionParameterRef
	@OneToMany(mappedBy = "combinationRevision")
	private List<CombinationRevisionParameterRef> combinationRevisionParameterRefs;

	// bi-directional many-to-one association to Combination
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "combination_id", nullable = false)
	private Combination combination;

}