package com.aptar.spc.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * The persistent class for the upload_excel database table.
 * 
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "upload_excel")
@NamedQuery(name = "UploadExcel.findAll", query = "SELECT u FROM UploadExcel u")
public class UploadExcel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "UPLOAD_EXCEL_ID_GENERATOR")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UPLOAD_EXCEL_ID_GENERATOR")
	private Integer id;

	@Lob
	private byte[] file;

	@CreatedBy
	private String createdby;

	@CreatedDate
	private Timestamp createdon;

	@LastModifiedBy
	private String updatedby;

	@LastModifiedDate
	private Timestamp updatedon;

	private Boolean recalculated;

	// bi-directional many-to-one association to CombinationRevisionParameterRef
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "combination_revision_parameter_ref_id")
	private CombinationRevisionParameterRef combinationRevisionParameterRef;

}