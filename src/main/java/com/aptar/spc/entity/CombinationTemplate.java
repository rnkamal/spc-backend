package com.aptar.spc.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * The persistent class for the combination_templates database table.
 * 
 */
@Data
@ToString
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "combination_templates")
@NamedQuery(name = "CombinationTemplate.findAll", query = "SELECT c FROM CombinationTemplate c")
public class CombinationTemplate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "COMBINATION_TEMPLATES_ID_GENERATOR")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMBINATION_TEMPLATES_ID_GENERATOR")
	@Column(unique = true, nullable = false)
	private Integer id;

	@Column(nullable = false)
	private Boolean active;

	@CreatedBy
	@Column(nullable = false, updatable = false)
	private String createdby;

	@CreatedDate
	@Column(nullable = false, updatable = false)
	private Timestamp createdon;

	@Column(length = 155)
	private String description;

	@Column(length = 100)
	private String name;

	@LastModifiedBy
	private String updatedby;

	@LastModifiedDate
	private Timestamp updatedon;

	// bi-directional many-to-one association to CombinationAttributeTemplate
	@OneToMany(mappedBy = "combinationTemplate")
	private List<CombinationAttributeTemplate> combinationAttributeTemplates;

	// bi-directional many-to-one association to MachineGroup
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "machinegroup_id")
	private MachineGroup machineGroup;

	// bi-directional many-to-one association to Combination
	@OneToMany(mappedBy = "combinationTemplate")
	private List<Combination> combinations;

}