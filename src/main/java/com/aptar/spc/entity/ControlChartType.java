package com.aptar.spc.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * The persistent class for the control_chart_type database table.
 * 
 */
@Data
@ToString
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "control_chart_type")
@NamedQuery(name = "ControlChartType.findAll", query = "SELECT c FROM ControlChartType c")
public class ControlChartType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "CONTROL_CHART_TYPE_ID_GENERATOR")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTROL_CHART_TYPE_ID_GENERATOR")
	@Column(unique = true, nullable = false)
	private Integer id;

	@Column(nullable = false)
	private Boolean active;

	@CreatedBy
	@Column(nullable = false, updatable = false)
	private String createdby;

	@CreatedDate
	@Column(nullable = false, updatable = false)
	private Timestamp createdon;

	@Column(nullable = false, length = 55)
	private String type;

	@LastModifiedBy
	private String updatedby;

	@LastModifiedDate
	private Timestamp updatedon;

	// bi-directional many-to-one association to Machine
	@OneToMany(mappedBy = "controlChartType")
	private List<Machine> machines;

	// bi-directional many-to-one association to Site
	@OneToMany(mappedBy = "controlChartType")
	private List<Site> sites;

	// bi-directional many-to-one association to Machine
	@OneToMany(mappedBy = "controlChartType")
	private List<Graph> graph;

}