package com.aptar.spc.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * The persistent class for the spc_rules database table.
 * 
 */
@Data
@ToString
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "spc_rules")
@NamedQuery(name = "SpcRule.findAll", query = "SELECT s FROM SpcRule s")
public class SpcRule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SPC_RULES_ID_GENERATOR")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SPC_RULES_ID_GENERATOR")
	@Column(unique = true)
	@NotBlank(message = "id is mandatory")
	private Integer id;

	@NotBlank(message = "active is mandatory")
	private Boolean active;

	@CreatedBy
	@Column(nullable = false, updatable = false)
	private String createdby;

	@CreatedDate
	@Column(nullable = false, updatable = false)
	private Timestamp createdon;

	@Max(value = 55, message = "description is must have size less than or equal to 55")
	private String description;

	@NotBlank(message = "rule is mandatory")
	@Max(value = 25, message = "rule is must have size less than or equal to 25")
	private String rule;

	@LastModifiedBy
	private String updatedby;

	@LastModifiedDate
	private Timestamp updatedon;

}