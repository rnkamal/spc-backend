package com.aptar.spc.validation;

import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;

import com.aptar.spc.validation.interfaces.OneOf;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OneOfValidator implements ConstraintValidator<OneOf, Object> {

	private String[] fields;
	private String condition;

	@Override
	public void initialize(OneOf annotation) {
		this.fields = annotation.value();
		this.condition = annotation.condition();
	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		log.info("--> OneOfValidator.isValid()");
		BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(value);
		boolean result = false;
		switch (condition.toLowerCase()) {
		case "notnull":
			result = checkNotnull(wrapper);
		}

		return result;
	}

	private int countNumberOfMatches(BeanWrapper wrapper) {
		log.info("--> OneOfValidator.countNumberOfMatches()");
		int matches = 0;
		for (String field : fields) {
			Object value = wrapper.getPropertyValue(field);
			boolean isPresent = detectOptionalValue(value);
			if (value instanceof List<?>) {
				if (!(((List) value).isEmpty()) && isPresent) {
					matches++;
				}
			}
		}
		return matches;
	}

	private boolean detectOptionalValue(Object value) {
		log.info("--> OneOfValidator.detectOptionalValue()");
		if (value instanceof Optional) {
			return ((Optional) value).isPresent();
		}
		return true;
	}

	private boolean checkNotnull(BeanWrapper wrapper) {
		log.info("--> OneOfValidator.checkNotnull()");
		int matches = countNumberOfMatches(wrapper);
		if (matches > 1) {
			return true;
		} else {
			return false;
		}
	};

}
