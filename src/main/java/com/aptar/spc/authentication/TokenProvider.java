package com.aptar.spc.authentication;
//package com.aptar.spc.common.authentication;
//
//import java.security.Key;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collection;
//import java.util.Date;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Locale;
//import java.util.stream.Collectors;
//
//import javax.annotation.PostConstruct;
//
//import org.apache.commons.lang.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.stereotype.Component;
//
//import com.aptar.mes.core.security.UserToken;
//import com.aptar.mes.data.service.UserService;
//import com.aptar.mes.security.config.MesSecurityProperties;
//
//import io.jsonwebtoken.Claims;
//import io.jsonwebtoken.ExpiredJwtException;
//import io.jsonwebtoken.Jwts;
//import io.jsonwebtoken.MalformedJwtException;
//import io.jsonwebtoken.SignatureAlgorithm;
//import io.jsonwebtoken.UnsupportedJwtException;
//import io.jsonwebtoken.security.Keys;
//import io.jsonwebtoken.security.SignatureException;
//
///**
// * This class is use to generate the JWT token. Token could be sotre in cookie @See {@link CookieJWTGenerator} or given with API
// */
//@Component
//@ConditionalOnProperty(name = "mes.security.enable", havingValue = "true")
//public class TokenProvider {
//
//	private static final int TIME_ONE_SECOND_IN_MILLIS = 1000;
//
//	private final Logger log = LoggerFactory.getLogger(TokenProvider.class);
//
//	private static final String AUTHORITIES_AUTHS = "auths";
//	private static final String AUTHORITIES_GROUPS = "groups";
//	private static final String CLAIM_ID = "id";
//	private static final String CLAIM_LOCALE = "locale";
//	private static final String CLAIM_DATE_FORMAT = "dateFormat";
//	private static final String CLAIM_DATE_TIME_FORMAT = "dateTimeFormat";
//
//	private long tokenValidityInMilliseconds;
//	private String secretKey;
//
//	private MesSecurityProperties mesSecurityProperties;
//
//	private UserService userService;
//
//	@Autowired
//	public TokenProvider(final MesSecurityProperties mesSecurityProperties, final UserService userService) {
//		this.mesSecurityProperties = mesSecurityProperties;
//		this.userService = userService;
//	}
//
//	@PostConstruct
//	public void init() {
//		this.tokenValidityInMilliseconds = TIME_ONE_SECOND_IN_MILLIS * mesSecurityProperties.getAuthentication().getJwt().getTokenValidityInSeconds();
//		this.secretKey = mesSecurityProperties.getAuthentication().getJwt().getSecret();
//
//	}
//
//	private Key getKey() {
//		return Keys.hmacShaKeyFor(secretKey.getBytes());
//	}
//
//	public String createTokenFromAuthentication(final Authentication authentication) {
//		UserToken userToken = (UserToken) authentication.getPrincipal();
//		if (userToken != null) {
//			long now = (new Date()).getTime();
//			Date validity = new Date(now + this.tokenValidityInMilliseconds);
//			return Jwts.builder()
//					.setSubject(userToken.getUsername())
//					.claim(CLAIM_ID, userToken.getId())
//					.claim(CLAIM_LOCALE, userToken.getLocale() != null ? userToken.getLocale().toLanguageTag() : null)
//					.claim(CLAIM_DATE_FORMAT, userToken.getDateFormat())
//					.claim(CLAIM_DATE_TIME_FORMAT, userToken.getDateTimeFormat())
//					.claim(AUTHORITIES_GROUPS, userToken.getGroups())
//					.signWith(getKey(), SignatureAlgorithm.HS512)
//					.setExpiration(validity)
//					.compact();
//		} else {
//			return null;
//		}
//	}
//
//	/**
//	 * Function used to generate an authentication token for one of our service or applications.
//	 */
//	public String createServiceToken(final String serviceName, final List<String> authorities) {
//		long now = (new Date()).getTime();
//		Date validity = new Date(now + this.tokenValidityInMilliseconds);
//
//		return Jwts.builder()
//				.setSubject(serviceName)
//				.claim(AUTHORITIES_AUTHS, String.join(",", authorities))
//				.signWith(getKey(), SignatureAlgorithm.HS512)
//				.setExpiration(validity)
//				.compact();
//	}
//
//	@SuppressWarnings("unchecked")
//	public Authentication getAuthentication(final String token) {
//		Claims claims = Jwts.parser()
//				.setSigningKey(getKey())
//				.parseClaimsJws(token)
//				.getBody();
//
//		UserToken userToken = new UserToken();
//		userToken.setUsername(claims.getSubject());
//		if (claims.containsKey(CLAIM_ID)) {
//			if (claims.get(CLAIM_ID) instanceof Integer) {
//				// Small long are convert to integer by jwt token
//				userToken.setId(((Integer) claims.get(CLAIM_ID)).longValue());
//			} else {
//				userToken.setId((Long) claims.get(CLAIM_ID));
//			}
//		}
//		if (claims.containsKey(CLAIM_LOCALE)) {
//			userToken.setLocale(Locale.forLanguageTag((String) claims.get(CLAIM_LOCALE)));
//		}
//		if (claims.containsKey(CLAIM_DATE_FORMAT)) {
//			userToken.setDateFormat((String) claims.get(CLAIM_DATE_FORMAT));
//		}
//		if (claims.containsKey(CLAIM_DATE_TIME_FORMAT)) {
//			userToken.setDateTimeFormat((String) claims.get(CLAIM_DATE_TIME_FORMAT));
//		}
//
//		Collection<GrantedAuthority> authorities = new ArrayList<>();
//		if (claims.containsKey(AUTHORITIES_GROUPS)) {
//			userToken.setGroups(new HashSet<>((List<String>) claims.get(AUTHORITIES_GROUPS)));
//			authorities.addAll(userService.findPrivilegesByGroups(userToken.getGroups()).stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
//		}
//		if (StringUtils.isNotBlank((String) claims.get(AUTHORITIES_AUTHS))) {
//			authorities.addAll(Arrays.stream(claims.get(AUTHORITIES_AUTHS).toString().split(","))
//					.map(SimpleGrantedAuthority::new)
//					.collect(Collectors.toList()));
//		}
//		return new UsernamePasswordAuthenticationToken(userToken, "", authorities);
//	}
//
//	public boolean validateToken(final String authToken) {
//		if (StringUtils.isNotBlank(authToken)) {
//			try {
//				Jwts.parser().setSigningKey(getKey()).parseClaimsJws(authToken).getBody();
//				return true;
//			} catch (ExpiredJwtException expiredJwtException) {
//				log.info("Token expired : {}", expiredJwtException.getMessage());
//				return false;
//			} catch (MalformedJwtException | UnsupportedJwtException e) {
//				log.warn("Invalid or Unsupported JWT token ", e);
//				return false;
//			} catch (SignatureException e) {
//				log.error("Invalid JWT signature: {}", e.getMessage());
//				return false;
//			}
//		} else {
//			log.error("JWT signature was blank");
//			return false;
//		}
//	}
//}
