package com.aptar.spc.constants;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.ImmutableList;

public final class Constants {

	public static final ImmutableList<String> COMBINATION_HEADER_CONSTANTS = ImmutableList.of("machine");
	public static final String PROD_ORD_HEADER = "prod.Ord";
	public static final String REVISION_PARAM_ID = "revisionParamId";
	public static final String SPC_STATUS_HEADER = "status";
	public static final String SPC_STATUS_ACTIVE = "Active";
	public static final String SPC_STATUS_INACTIVE = "InActive";
	public static final String SPC_STATUS_ACTIVE_WITH_ALERT = "ActiveWithAlert";
	public static final String MACHINE_TAG = "machineTag";
	public static final String MACHINE_NAME = "machineName";

	public static final String HISTORIAN_SERVER_NAME = "SPESHST01.aptargroup.loc";
	public static final String URL = "https://" + HISTORIAN_SERVER_NAME + "/piwebapi";
	public static final String USERNAME = "aptargroup\\SA-MESWebAPISPC";
	public static final String PASSWORD = "AptarSPC1000";

	public static final Map<String, Integer> MACHINE_STATUS_MAP;
	static {
		Map<String, Integer> Machine_Status_Map = new HashMap<>();
		Machine_Status_Map.put(Constants.SPC_STATUS_ACTIVE_WITH_ALERT, 1);
		Machine_Status_Map.put(Constants.SPC_STATUS_ACTIVE, 2);
		Machine_Status_Map.put(Constants.SPC_STATUS_INACTIVE, 3);
		MACHINE_STATUS_MAP = Collections.unmodifiableMap(Machine_Status_Map);
	}
}
