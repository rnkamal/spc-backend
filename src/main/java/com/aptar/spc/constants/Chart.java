package com.aptar.spc.constants;

public enum Chart {
	i("Individuals"), mr("MR"), xbarr("RXBar"), xbars("SXBar"), s("StdDev"), r("Range");

	private final String chart;

	Chart(String chart) {
		this.chart = chart;
	}

	public String get() {
		return chart;
	}
}
