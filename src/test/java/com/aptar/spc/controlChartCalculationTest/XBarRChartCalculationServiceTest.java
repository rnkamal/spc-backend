package com.aptar.spc.controlChartCalculationTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.aptar.spc.DTO.response.ControlChartResponse;
import com.aptar.spc.service.controlchartservice.XBarRChartCalculationService;

@SpringBootTest
class XBarRChartCalculationServiceTest {

	@Autowired
	XBarRChartCalculationService xBarRChartCalculationService;

	public static final double[] attributeValue = new double[] { 2, 14, 36, 8, 12, 64, 26, 18, 23, 24 };

	@Test
	void testSelf() {
		assertTrue(xBarRChartCalculationService.self() instanceof XBarRChartCalculationService);
	}

	@Test
	void testGetControlLimit() {
		ControlChartResponse model = xBarRChartCalculationService.buildSubGroups(attributeValue, 5).getControlLimit();
		assertTrue(!(model.getUcl_R().isEmpty()));
		assertTrue(!(model.getLcl_R().isEmpty()));
		assertTrue(!(model.getUcl_XBar().isEmpty()));
		assertTrue(!(model.getLcl_XBar().isEmpty()));
	}

}
