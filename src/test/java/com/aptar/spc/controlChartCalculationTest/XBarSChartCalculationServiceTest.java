package com.aptar.spc.controlChartCalculationTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.aptar.spc.DTO.response.ControlChartResponse;
import com.aptar.spc.service.controlchartservice.XBarSChartCalculationService;

@SpringBootTest
class XBarSChartCalculationServiceTest {

	@Autowired
	XBarSChartCalculationService xBarSChartCalculationService;

	public static final double[] attributeValue = new double[] { 2, 14, 36, 8, 12, 64, 26, 18, 23, 24 };

	@Test
	void testSelf() {
		assertTrue(xBarSChartCalculationService.self() instanceof XBarSChartCalculationService);
	}

	@Test
	void testGetControlLimit() {
		ControlChartResponse model = xBarSChartCalculationService.buildSubGroups(attributeValue, 5).getControlLimit();
		assertTrue(!(model.getUcl_S().isEmpty()));
		assertTrue(!(model.getLcl_S().isEmpty()));
		assertTrue(!(model.getUcl_XBar().isEmpty()));
		assertTrue(!(model.getLcl_XBar().isEmpty()));
	}

}
