package com.aptar.spc.controlChartCalculationTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.aptar.spc.DTO.response.ControlChartResponse;
import com.aptar.spc.service.controlchartservice.IMRChartCalculationService;

@SpringBootTest
class IMRChartCalculationServiceTests {

	@Autowired
	IMRChartCalculationService iMRChartCalculationService;

	public static final double[] attributeValue = new double[] { 2, 14, 36, 8, 12, 64, 26, 18, 23, 24 };

	@Test
	void testSelf() {
		assertTrue(iMRChartCalculationService.self() instanceof IMRChartCalculationService);
	}

	@Test
	void testGetControlLimit() {
		ControlChartResponse model = iMRChartCalculationService.loadAttirbute(attributeValue).getControlLimit();
		assertTrue(!(model.getUcl_I().isEmpty()));
		assertTrue(!(model.getLcl_I().isEmpty()));
		assertTrue(!(model.getUcl_MR().isEmpty()));
		assertTrue(!(model.getLcl_MR().isEmpty()));
	}

}
