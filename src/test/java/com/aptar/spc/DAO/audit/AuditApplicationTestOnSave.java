package com.aptar.spc.DAO.audit;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.aptar.spc.DAO.UsersDAO;
import com.aptar.spc.entity.User;

@Transactional
@SpringBootTest
public class AuditApplicationTestOnSave {

	@Autowired
	private UsersDAO usersDAO;

	private User user;

	@Test
	public void create() {
		user = usersDAO.save(User.builder().firstname("Rashidi Zin").lastname("rashidi.zin").build());

		assertThat(user.getCreatedby()).isNotNull();

		assertThat(user.getUpdatedby()).isNotNull();

//		assertThat(user.getCreatedby()).isEqualTo(2);
//
//		assertThat(user.getUpdatedby()).isEqualTo(2);
	}
}
