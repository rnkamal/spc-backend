package com.aptar.spc.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.aptar.spc.service.DAOservice.RevisionParamService;
import com.aptar.spc.service.DAOservice.UploadExcelService;

import lombok.extern.slf4j.Slf4j;

@WebMvcTest(controllers = CombinationRevisionParamController.class)
@AutoConfigureMockMvc
@Slf4j
public class CombinationRevisionParamControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private RevisionParamService RevisionParamService;

	@MockBean
	private UploadExcelService UploadExcelService;

	@Test
	public void whenGetRequestToGetCombinationRevisionParamsById_thenCorrectResponse() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/combination/revision/{combinationRevisionId}/param", 1))
				.andExpect(status().isOk()).andReturn();
		assertEquals("[]", mvcResult.getResponse().getContentAsString());
	}

	@Test
	public void whenGetRequestToGetRevisionParam_thenCorrectResponse() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/combination/revision/params/{revision-param-id}", 1))
				.andExpect(status().isOk()).andReturn();
		assertEquals("", mvcResult.getResponse().getContentAsString());
	}
}
