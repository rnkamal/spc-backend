package com.aptar.spc.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.aptar.spc.DTO.request.CombinationRequest;
import com.aptar.spc.DTO.response.CombinationResponse;
import com.aptar.spc.service.DAOservice.CombinationService;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@WebMvcTest(controllers = CombinationController.class)
@AutoConfigureMockMvc
@Slf4j
public class CombinationControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private CombinationService combinationService;

	@Autowired
	CombinationController combinationController;

	@Test
	public void whenPostRequestTogetAllCombination_thenCorrectResponse() {
		String combinationRequest = "{\"site\": [1], \"functionalLocation\" : [1]}";
		try {
			mockMvc.perform(MockMvcRequestBuilders.post("/combination/list").content(combinationRequest)
					.contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());
		} catch (Exception e) {
			log.warn(e.getLocalizedMessage());
		}
	}

	@Test
	public void getAllCombinationTest() {
		CombinationResponse combinationResponses = CombinationResponse.builder().attributes(null)
				.combinationResponse(null).build();
		CombinationRequest combinationRequest = CombinationRequest.builder().site(new ArrayList<>(Arrays.asList(1)))
				.functionalLocation(new ArrayList<>(Arrays.asList(1))).build();
		Mockito.when(combinationService.getAllCombination(new ArrayList<>(Arrays.asList(1)),
				new ArrayList<>(Arrays.asList(1)))).thenReturn(combinationResponses);
		String url = "/combination/list";
		try {
			MvcResult mvcResult = mockMvc.perform(post(url).content(objectMapper.writeValueAsString(combinationRequest))
					.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
			String actualReponse = mvcResult.getResponse().getContentAsString();
			String ExpectedResponse = objectMapper.writeValueAsString(combinationResponses);
			assertThat(actualReponse).isEqualToIgnoringWhitespace(ExpectedResponse);
		} catch (Exception e) {
			log.warn(e.getLocalizedMessage());
		}
	}
}
