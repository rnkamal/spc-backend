package com.aptar.spc.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.aptar.spc.service.DAOservice.CombinationRevisionService;

import lombok.extern.slf4j.Slf4j;

@WebMvcTest(controllers = CombinationRevisionController.class)
@AutoConfigureMockMvc
@Slf4j
public class CombinationRevisionControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CombinationRevisionService CombinationRevisionService;

	@Test
	public void whenGetRequestToGetActiveCombinationRevision_thenCorrectResponse() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/combination/{combinationId}/revision/active/param", 1))
				.andExpect(status().isOk()).andReturn();
		assertEquals("", mvcResult.getResponse().getContentAsString());
	}

	@Test
	public void whenGetRequestToGetAllCombinationRevision_thenCorrectResponse() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/combination/{combinationId}/revision/all", 1))
				.andExpect(status().isOk()).andReturn();
		assertEquals("[]", mvcResult.getResponse().getContentAsString());
	}

	@Test
	public void whenGetRequestToActivateCombinationRevisionById_thenCorrectResponse() throws Exception {
		MvcResult mvcResult = mockMvc.perform(put("/combination/revision/{combinationRevisionId}/activate", 1))
				.andExpect(status().isOk()).andReturn();
		assertEquals("", mvcResult.getResponse().getContentAsString());
	}

}
