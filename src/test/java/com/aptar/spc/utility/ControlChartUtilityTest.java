package com.aptar.spc.utility;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.aptar.spc.util.ControlChartUtility;

@SpringBootTest
public class ControlChartUtilityTest {

	public static final double[] sampleArray = new double[] { 2, 14, 36, 8, 12, 64, 26, 18, 23, 24 };

	@Test
	void testSplitArray() {
		int subGroup = 3;
		HashMap<Integer, Object> output = ControlChartUtility.splitArray(sampleArray, subGroup);
		output.forEach((k, v) -> {
			double[] splittedArray = (double[]) v;
			assertTrue(splittedArray.length <= subGroup);
		});
	}

}
